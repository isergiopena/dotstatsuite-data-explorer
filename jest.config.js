module.exports = {
  clearMocks: true,
  coverageReporters: ['text'],
  coverageDirectory: 'coverage',
  collectCoverageFrom: [
    '!src/proxy/**',
    '!src/mock/**',
    '!src/**/*.test.{js,jsx}',
    '!**/*e2e.js',
    'src/**/*.js',
  ],
  testRegex: 'src/.*\\.test\\.js$',
  testEnvironment: 'jsdom',
  testURL: 'http://localhost',
  /* transform: {
    '\\.[jt]sx?$': 'babel-jest',
    '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
    '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)':'<rootDir>/config/jest/fileTransform.js',
  },
  transformIgnorePatterns: [
    '/node_modules/',
    '\\.pnp\\.[^\\/]+$',
    '^.+\\.module\\.(css|sass|scss)$',
  ], */
  transform: {
    '\\.[jt]sx?$': 'babel-jest',
    '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
    // to disable for server test
    //'^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': '<rootDir>/config/jest/fileTransform.js',

    '^.+\\.png$': '<rootDir>/config/jest/fileTransform.js',
  },
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
    '^.+\\.module\\.(css|sass|scss)$',
  ],
};
