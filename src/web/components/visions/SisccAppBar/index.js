import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import * as R from 'ramda';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import LanguageIcon from '@material-ui/icons/Language';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Typography, useTheme } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import { useIntl } from 'react-intl';
import { formatMessage, FormattedMessage } from '../../../i18n';
import { hasContactForm } from '../../../lib/settings';
import User from '../../user';
import Contact from '../../contact';
import AccessibilityButton from '../../AccessibilityButton';
import ReducedActionsMenu from './ReducedMenuActions';
import useStyles from './useStyles';
import { messages as i18nMessages } from '../../../i18n/messages';
import { useOidc } from '../../../lib/oidc/index';

const Input = id => {
  const intl = useIntl();
  const classes = useStyles();
  const theme = useTheme();
  const isXs = useMediaQuery(theme.breakpoints.down('xs'));

  if (isXs) {
    return (
      <React.Fragment>
        <LanguageIcon className={classes.value} color="primary" />
        {R.has(id, i18nMessages) ? formatMessage(intl)(i18nMessages[id]) : id}
      </React.Fragment>
    );
  }
  return (
    <span className={classes.value}>
      <LanguageIcon color="primary" />
      {R.has(id, i18nMessages) ? formatMessage(intl)(i18nMessages[id]) : id}
    </span>
  );
};

Input.propTypes = {
  id: PropTypes.string,
};

const Component = ({
  logo,
  locales,
  localeId,
  changeLocale,
  logoLink,
  isFixed,
}) => {
  const classes = useStyles();
  const intl = useIntl();
  const auth = useOidc();
  const theme = useTheme();
  const isSm = useMediaQuery(theme.breakpoints.down('sm'));
  const isXs = useMediaQuery(theme.breakpoints.down('xs'));

  return (
    <AppBar
      data-testid="sisccappbar"
      position="static"
      className={cx(classes.appBar, { [classes.fixed]: isFixed })}
      elevation={0}
    >
      <Toolbar className={classes.toolBar}>
        <div className={classes.logoWrapper}>
          {logoLink ? (
            <Link href={logoLink} target="_blank" rel="noopener noreferrer">
              {' '}
              <img
                className={cx(classes.logo, classes.alternativeBrowserLogo)}
                src={logo}
                alt="siscc logo"
              />{' '}
            </Link>
          ) : (
            <img
              className={cx(classes.logo, classes.alternativeBrowserLogo)}
              src={logo}
              alt="siscc logo"
            />
          )}
        </div>
        {!R.isEmpty(R.path([localeId, 'de.header.message'])(window.I18N)) &&
          !isSm && (
            <>
              <Typography variant="body1" className={classes.headerTitle}>
                <FormattedMessage id="de.header.message" />
              </Typography>
              <Divider
                orientation="vertical"
                flexItem
                className={classes.divider}
              />
            </>
          )}
        {!isXs && (
          <React.Fragment>
            <AccessibilityButton />
            {auth && (
              <React.Fragment>
                <Divider
                  orientation="vertical"
                  flexItem
                  className={classes.divider}
                />
                <User />
              </React.Fragment>
            )}
            {hasContactForm && (
              <React.Fragment>
                <Divider
                  orientation="vertical"
                  flexItem
                  className={classes.divider}
                />
                <Contact />
              </React.Fragment>
            )}
            <Divider
              orientation="vertical"
              flexItem
              className={classes.divider}
            />
            <TextField
              select
              id="languages" // TextField should always have id (accessibility)
              className={classes.textField}
              value={localeId}
              variant="outlined"
              onChange={event => changeLocale(event.target.value)}
              inputProps={{
                renderValue: Input,
              }}
              SelectProps={{
                classes: {
                  root: classes.select,
                  iconOutlined: classes.arrowDown,
                },
                MenuProps: {
                  getContentAnchorEl: null,
                  anchorOrigin: { vertical: 'bottom', horizontal: 'center' },
                  transformOrigin: { vertical: 'top', horizontal: 'center' },
                  classes: { paper: classes.paper },
                },
              }}
            >
              {R.map(id => (
                <MenuItem
                  key={id}
                  id={id}
                  value={id}
                  dense
                  className={classes.menuItem}
                >
                  <span>
                    {R.has(id, i18nMessages)
                      ? formatMessage(intl)(i18nMessages[id])
                      : id}
                  </span>
                </MenuItem>
              ))(locales)}
            </TextField>
          </React.Fragment>
        )}
        {isXs && (
          <ReducedActionsMenu
            localeId={localeId}
            locales={locales}
            changeLocale={changeLocale}
          />
        )}
      </Toolbar>
      {!R.isEmpty(R.path([localeId, 'de.header.message'])(window.I18N)) &&
        isSm && (
          <Toolbar className={classes.headerNote}>
            <Typography variant="body1" className={classes.headerTitle}>
              <FormattedMessage id="de.header.message" />
            </Typography>
          </Toolbar>
        )}
    </AppBar>
  );
};

Component.propTypes = {
  logo: PropTypes.string,
  locales: PropTypes.array.isRequired,
  localeId: PropTypes.string,
  changeLocale: PropTypes.func.isRequired,
  logoLink: PropTypes.string,
  isFixed: PropTypes.bool,
};

export default Component;
