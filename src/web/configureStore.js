import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { createRouterMiddleware } from '@lagunovsky/redux-react-router';
import createRootReducer from './reducers';
import rootSaga from './sagas';
import { analyticsMiddleware, historyMiddleware } from './middlewares';
import { getHasToken } from './utils/analytics';

const isDev = process.env.NODE_ENV === 'development';

const isInBrowser = typeof window !== 'undefined';
export const history = isInBrowser ? createBrowserHistory() : {};

export default initialState => {
  const composeEnhancer =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [
    thunk,
    sagaMiddleware,
    historyMiddleware,
    createRouterMiddleware(history),
  ];

  if (getHasToken()) middlewares.push(analyticsMiddleware);

  if (isDev) {
    const { createLogger } = require('redux-logger');
    const logger = createLogger({
      duration: true,
      timestamp: false,
      collapsed: true,
      diff: true,
    });
    middlewares.push(logger);
  }

  const store = createStore(
    createRootReducer(history),
    initialState,
    composeEnhancer(applyMiddleware(...middlewares)),
  );

  sagaMiddleware.run(rootSaga);
  return store;
};
