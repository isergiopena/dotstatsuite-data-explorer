const initAssets = require('./init/assets');
const initRouter = require('./init/router');
const initHttp = require('./init/http');
const initServices = require('./services');
const initProxyServer = require('./init/proxyServer');
const { initResources } = require('./utils');

const ressources = [
  initAssets,
  initServices,
  initRouter,
  initHttp,
  initProxyServer,
];

module.exports = config => initResources(ressources)(config);
