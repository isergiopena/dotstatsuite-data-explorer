import { changeVibe, testidSelector } from './utils';
import { map, nth, pipe, splitEvery, unnest } from 'ramda';

describe('microdata table', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('ddown');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should display disabled microdata button in toolbar', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const hasDisabledMicrodataButton = await toolbar.$eval(
      '[id="microdata"]',
      button => button.disabled,
    );
    expect(hasDisabledMicrodataButton).toEqual(true);
  });
  it('click obs cell hyperlink', async () => {
    const cell = await page.$('td[headers="REF_AREA=TUR_0_0 2_0 MEASURE=GDO"]');
    const hyperlink = await cell.$('.MuiLink-underlineAlways');
    await hyperlink.click();
  });
  it('should display microdata table', async () => {
    await page.waitForSelector(testidSelector('microdata-table'));
  });
  it('microdata button is not disabled anymore', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const hasDisabledMicrodataButton = await toolbar.$eval(
      '[id="microdata"]',
      button => button.disabled,
    );
    expect(hasDisabledMicrodataButton).toEqual(false);
  });
  it('go back to table', async () => {
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
  });
  it('previous table should be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('microdata button is still not disabled', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const hasDisabledMicrodataButton = await toolbar.$eval(
      '[id="microdata"]',
      button => button.disabled,
    );
    expect(hasDisabledMicrodataButton).toEqual(false);
  });
  it('microdata button click', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const microdataButton = await toolbar.$('[id="microdata"]');
    await microdataButton.click();
  });
  it('should return to microdata table', async () => {
    await page.waitForSelector(testidSelector('microdata-table'));
  });
  it('microdata hierarchical content', async () => {
    const table = await page.$(testidSelector('microdata-table'));
    const tableContent = await table.$$eval('td', nodes =>
      nodes.map(n => n.textContent),
    );
    const hierarchicalColumn = pipe(
      splitEvery(10),
      map(nth(4)),
      unnest,
    )(tableContent);
    expect(hierarchicalColumn).toEqual([
      'ROOT_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  LEVEL_1_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  ·  LEVEL_2_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  ·  LEVEL_2_VALUE_2',
      // eslint-disable-next-line no-irregular-whitespace
      '·  LEVEL_1_VALUE_2',
      'ROOT_VALUE_2',
      'ROOT_VALUE_3',
      'ROOT_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  LEVEL_1_VALUE_1',
      // eslint-disable-next-line no-irregular-whitespace
      '·  ·  LEVEL_2_VALUE_1',
    ]);
  });
});
