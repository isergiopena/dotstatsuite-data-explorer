const express = require('express');
const proxy = require('http-proxy-middleware');
const host = 'localhost';

const filterApi = () => true;
const filterConfig = (pathname, req) => {
  if (req.url.match('^/assets')) return true;
  return false;
};

module.exports = ctx => {
  const {
    httpServer,
    config: { configUrl },
  } = ctx;
  const app = express();

  const configOptions = { target: configUrl };
  const proxyConfig = proxy(filterConfig, configOptions);
  app.use(proxyConfig);

  const apiOptions = { target: httpServer.url };
  const proxyApi = proxy(filterApi, apiOptions);
  app.use(proxyApi);

  return new Promise((resolve, reject) => {
    const server = app.listen(null, host, err => {
      if (err) return reject(err);
      const { port: sport } = server.address();
      server.url = `http://${host}:${sport}`;
      return resolve({ ...ctx, proxyServer: server });
    });
  });
};
