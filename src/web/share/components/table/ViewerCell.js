import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import IconButton from '@material-ui/core/IconButton';
import { CopyContent as CopyContentIcon } from '@sis-cc/dotstatsuite-visions';

const useStyles = makeStyles(theme => ({
  select: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
  },
  icon: {
    cursor: 'pointer',
    verticalAlign: 'bottom',
  },
}));

const ViewerCell = ({ viewerUrl }) => {
  const classes = useStyles();
  const [copied, setCopied] = useState(false);
  const copyToClipboard = () => {
    navigator.clipboard.writeText(viewerUrl);
    setCopied(true), setTimeout(() => setCopied(false), 200);
  };

  return (
    <Fragment>
      <Link color="primary" href={viewerUrl} className={cx({ [classes.select]: copied })}>
        {viewerUrl}
      </Link>
      &nbsp;
      <IconButton
        component={Link}
        href={viewerUrl}
        target="_blank"
        color="primary"
        rel="noopener noreferrer"
        size="small"
      >
        <OpenInNewIcon color="primary" fontSize="small" className={classes.icon} />
      </IconButton>
      &nbsp;
      <IconButton size="small" onClick={copyToClipboard}>
        <CopyContentIcon color="primary" className={classes.icon} fontSize="small" />
      </IconButton>
    </Fragment>
  );
};

ViewerCell.propTypes = {
  viewerUrl: PropTypes.string,
};

export default ViewerCell;
