import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';

const Cell = withStyles(theme => ({
  root: {
    padding: theme.spacing(1),
  },
}))(TableCell);

export default Cell;
