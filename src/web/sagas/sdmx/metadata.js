import { call, select, put, takeLatest } from 'redux-saga/effects';
import * as R from 'ramda';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import {
  TOGGLE_METADATA,
  DOWNLOAD_METADATA,
  requestMetadata,
  handleMetadata,
  REQUEST_METADATA_ERROR,
  DOWNLOAD_METADATA_SUCCESS,
  HANDLE_MSD,
} from '../../ducks/metadata';
import { getDisplay, getLocale } from '../../selectors/router';
import {
  getRawStructureRequestArgs,
  getTimePeriodArtefact,
} from '../../selectors/sdmx';
import {
  getCoordinates,
  getMSD,
  getMSDRequestArgs,
} from '../../selectors/metadata';
import { getDataDimensions } from '../../selectors';
import { downloadFile } from './data';
import { getMetadataCoordinates } from '../../selectors/data';
import sdmxApi from '../../api/sdmx';
import { withAuthorizationHeaderGenerator } from './utils';

const getFilename = (dataquery, identifiers) => {
  return `${identifiers.agencyId}_${identifiers.code}_${identifiers.version}_${dataquery}_metadata`;
};

const getPartialDataquery = (indexedDimValIds, dimensions, timeArtefactId) => {
  return R.pipe(
    R.filter(d => d.id !== timeArtefactId),
    R.map(dim => {
      const values = dim.values;
      if (R.length(values) === 1) {
        return R.prop('id', R.head(values));
      }
      if (!R.has(dim.id, indexedDimValIds)) {
        return '*';
      }
      return R.propOr('*', dim.id, indexedDimValIds);
    }),
    R.join('.'),
  )(dimensions);
};

const singleTimeSelection = value => `ge:${value}+le:${value}`;

const getMetadataTimePeriodSelection = (coordinates, timeArtefact) => {
  if (R.isNil(timeArtefact)) {
    return {};
  }

  const periodSelection = R.has(timeArtefact.id, coordinates)
    ? singleTimeSelection(R.prop(timeArtefact.id, coordinates))
    : null;

  if (R.isNil(periodSelection)) {
    return {};
  }

  return { [`c[${timeArtefact.id}]`]: periodSelection };
};

function* metadataCtxGenerator({ format }) {
  const { datasource, identifiers } = yield select(getRawStructureRequestArgs);
  if (R.isNil(datasource?.urlv3)) return { disable: true };

  const locale = yield select(getLocale);
  const coordinates = yield select(getCoordinates);
  const timePeriodArtefact = yield select(getTimePeriodArtefact);
  const dimensions = yield select(getDataDimensions());

  const periodSelection = getMetadataTimePeriodSelection(
    coordinates,
    timePeriodArtefact,
  );
  const dataquery = getPartialDataquery(
    coordinates,
    dimensions,
    R.prop('id', timePeriodArtefact),
  );

  const customHeaders = R.propOr({}, 'headersv3', datasource);
  const acceptFormat =
    format === 'csv'
      ? R.pathOr(
          rules2.SDMX_3_0_CSV_DATA_FORMAT,
          ['metadata', format],
          customHeaders,
        )
      : R.pathOr(
          rules2.SDMX_3_0_JSON_DATA_FORMAT,
          ['metadata', format],
          customHeaders,
        );
  const requestArgs = {
    headers: {
      Accept: acceptFormat,
      'x-level': 'upperOnly',
      'accept-language': locale,
    },
    url: `${datasource.urlv3}/data/dataflow/${identifiers.agencyId}/${identifiers.code}/${identifiers.version}/${dataquery}`,
    params: { ...periodSelection, attributes: 'msd', measures: 'none' },
    responseType: format === 'csv' ? 'text' : null,
  };

  return { requestArgs, dataquery, locale, dimensions, identifiers };
}

function* requestMetadataWorker(action) {
  const ctx = yield call(metadataCtxGenerator, { format: 'json' });
  if (ctx.disable) return;

  const { coordinates, hasMetadata } = action.payload;

  if (!hasMetadata) {
    const metadataCoordinates = yield select(getMetadataCoordinates);
    const hasUpperMetadata = R.pipe(
      R.find(_coordinates => {
        const mergedCoord = R.mergeLeft(_coordinates, coordinates);
        return R.equals(mergedCoord, coordinates);
      }),
      R.complement(R.isNil),
    )(metadataCoordinates);
    if (!hasUpperMetadata) return;
  }

  yield put(requestMetadata());
  const requestArgs = yield call(
    withAuthorizationHeaderGenerator,
    ctx.requestArgs,
  );

  try {
    const response = yield call(sdmxApi, {
      method: 'getMetadata',
      requestArgs,
    });

    const msd = yield select(getMSD);
    if (R.isNil(msd)) {
      try {
        const msdRawRequestArgs = yield select(getMSDRequestArgs);
        const msdRequestArgs = yield call(
          withAuthorizationHeaderGenerator,
          msdRawRequestArgs,
        );
        const msdResponse = yield call(sdmxApi, {
          method: 'getMetadata',
          requestArgs: msdRequestArgs,
        });
        const msd = rules2.getMSDInformations(msdResponse.data);
        yield put({ type: HANDLE_MSD, payload: { msd } });
      } catch (error) {
        console.log('request msd error', error); // eslint-disable-line no-console
      }
    }

    const { attributes } = yield select(getMSD);
    const display = yield select(getDisplay);
    const metadataSeries = rules2.parseMetadataSeries(response.data, {
      display,
      locale: ctx.locale,
      dimensions: ctx.dimensions,
      attributes,
    });
    yield put(handleMetadata(metadataSeries));
  } catch (error) {
    yield put({ type: REQUEST_METADATA_ERROR, payload: { error } });
  }
}

function* downloadMetadataWorker() {
  const ctx = yield call(metadataCtxGenerator, { format: 'csv' });
  if (ctx.disable) return;

  const requestArgs = yield call(
    withAuthorizationHeaderGenerator,
    ctx.requestArgs,
  );
  const response = yield call(sdmxApi, { method: 'getMetadata', requestArgs });

  const filename = getFilename(ctx.dataquery, ctx.identifiers);
  yield call(downloadFile, { response, filename });

  yield put({ type: DOWNLOAD_METADATA_SUCCESS });
}

export function* watchRequestMetadata() {
  yield takeLatest(TOGGLE_METADATA, requestMetadataWorker);
  yield takeLatest(DOWNLOAD_METADATA, downloadMetadataWorker);
}
