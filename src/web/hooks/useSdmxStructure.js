import { useDispatch, useSelector } from 'react-redux';
import { getStructureRequestArgs } from '../selectors/sdmx';
import { PARSE_STRUCTURE } from '../ducks/sdmx';
import useSdmxQuery from './useSdmxQuery';

export default () => {
  const dispatch = useDispatch();

  const requestArgs = useSelector(getStructureRequestArgs);

  const ctx = { method: 'getStructure', requestArgs };

  const { isLoading, data, isError, error } = useSdmxQuery(ctx, {
    successHandler: ({ data, queryKey }) => {
      dispatch({
        type: PARSE_STRUCTURE,
        structure: data,
        queryKey,
      });
    },
  });

  return { isLoading, data, isError, error };
};
