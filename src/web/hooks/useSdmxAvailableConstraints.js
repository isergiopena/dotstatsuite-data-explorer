import { useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda';
import { getDimensions, getAvailableConstraintsArgs } from '../selectors/sdmx';
import { HANDLE_AVAILABLE_CONSTRAINTS } from '../ducks/sdmx';
import {
  parseActualContentConstraints,
  getConstraintsObservationsCount,
} from '@sis-cc/dotstatsuite-sdmxjs';
import useSdmxQuery from './useSdmxQuery';

const actionHandlerFactory = data => ({
  type: HANDLE_AVAILABLE_CONSTRAINTS,
  availableConstraints: parseActualContentConstraints()(data),
  dataPoints: getConstraintsObservationsCount(data),
});

export default () => {
  const dispatch = useDispatch();

  const dimensions = useSelector(getDimensions);
  const requestArgs = useSelector(getAvailableConstraintsArgs);
  const ctx = { method: 'getAvailableConstraints', requestArgs };

  const query = useSdmxQuery(ctx, {
    successHandler: ({ data }) => dispatch(actionHandlerFactory(data)),
    errorHandler: () => dispatch(actionHandlerFactory()),
    isEnabled: !R.all(R.isEmpty)(dimensions),
  });

  return query;
};
