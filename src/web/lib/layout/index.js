import * as R from 'ramda';
import {
  getRefAreaDimension,
  getTimePeriodDimension,
  isTimePeriodDimension,
} from '@sis-cc/dotstatsuite-sdmxjs';

const nbValues = 3; // specific rule, is it the number max of dimension values (table preview)

export const defaultLayoutBuilder = dimensions => {
  const flatDimensions = R.values(dimensions);
  const timeId = R.pipe(
    getTimePeriodDimension,
    R.when(R.identity, R.prop('id')),
  )({ dimensions: flatDimensions });
  const areaId = R.pipe(
    getRefAreaDimension,
    R.when(R.identity, R.prop('id')),
  )({ dimensions: flatDimensions });
  const helper = (omitIds, pickId) =>
    R.pipe(
      R.omit(omitIds),
      R.ifElse(R.has(pickId), R.prop(pickId), R.pipe(R.values, R.head)),
      R.prop('id'),
    );
  const x = helper([], areaId)(dimensions);
  const y = helper([areaId, x], timeId)(dimensions);
  const z = R.pipe(R.omit([x, y]), R.values, R.pluck('id'))(dimensions);

  return {
    rows: R.ifElse(R.isNil, R.always([]), R.flip(R.append)([]))(x),
    header: R.ifElse(R.isNil, R.always([]), R.flip(R.append)([]))(y),
    sections: z,
  };
};

export const getValuesFlat = R.pipe(R.values, R.flatten);

export const compactLayout = ({ many = {} } = {}) => layout => {
  if (R.isNil(layout)) return layout;
  const idsNotInMany = R.difference(getValuesFlat(layout), R.keys(many));
  return R.map(R.flip(R.difference)(idsNotInMany))(layout);
};

const getValues = dim =>
  R.pipe(
    R.propOr([], 'values'),
    R.take(nbValues),
    R.length,
    R.addIndex(R.times)((_, n) => ({ id: `${dim.id}-${n}`, label: 'Xxxx' })),
  )(dim);

export const getTableConfigLayout = layout => {
  const parseDimension = dimension => {
    const values = getValues(dimension);

    return {
      ...dimension,
      values,
      value: dimension.id,
      isTimePeriod: isTimePeriodDimension(dimension),
    };
  };

  return R.mapObjIndexed(subLayout => {
    return R.reduce(
      (acc, entry) => {
        if (R.has('dimensions', entry)) {
          const dimensions = R.map(parseDimension, entry.dimensions);
          return R.concat(acc, dimensions);
        }
        return R.append(parseDimension(entry), acc);
      },
      [],
      subLayout,
    );
  }, layout);
};

export const getLayout = (layoutIds, dimensions) => {
  const indexedDimensions = R.indexBy(R.prop('id'), dimensions);
  const refinedLayoutIds = R.mapObjIndexed(
    R.filter(id => R.has(id, indexedDimensions)),
    layoutIds,
  );
  const idsInLayout = getValuesFlat(refinedLayoutIds);
  if (!R.isEmpty(idsInLayout)) {
    const missingIds = R.pipe(
      R.filter(dim => !R.includes(dim.id, idsInLayout)),
      R.pluck('id'),
    )(dimensions);
    return R.pipe(
      R.over(R.lensProp('sections'), ids => R.concat(ids, missingIds)),
      R.when(
        layout => R.isEmpty(layout.rows),
        layout => {
          const toTakeInto = R.isEmpty(layout.sections) ? 'header' : 'sections';
          return {
            ...layout,
            rows: [R.path([toTakeInto, 0], layout)],
            [toTakeInto]: R.tail(R.prop(toTakeInto, layout)),
          };
        },
      ),
    )(refinedLayoutIds);
  }
  return defaultLayoutBuilder(indexedDimensions);
};
