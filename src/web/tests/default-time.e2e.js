import { changeVibe, testidSelector } from './utils';

describe('default time periods selections', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('ddown');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should have default start period 2020 selection from structure in url', async () => {
    const url = await page.url();
    expect(url).toContain('pd=2020%2C');
  });
  it('load url with empty time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&pd=%2C`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('time selection should be kept empty', async () => {
    const url = await page.url();
    expect(url).toContain('pd=%2C');
  });
  it('load url with lastnperiods', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=5&lom=LASTNPERIODS`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('no time selection should be in url and lastNPeriods kept', async () => {
    const url = await page.url();
    expect(url).not.toContain('pd');
    expect(url).toContain('lo=5&lom=LASTNPERIODS');
  });
  it('load url with lastnobservations and without time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=1&lom=LASTNOBSERVATIONS`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('no time selection should be in url and lastNObservations kept', async () => {
    const url = await page.url();
    expect(url).not.toContain('pd');
    expect(url).toContain('lo=1&lom=LASTNOBSERVATIONS');
  });
  it('load url with lastnobservations and custom time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=1&lom=LASTNOBSERVATIONS&pd=2017%2C2020`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('time selection and lastNObservations should be unchanged', async () => {
    const url = await page.url();
    expect(url).toContain('pd=2017%2C2020');
    expect(url).toContain('lo=1&lom=LASTNOBSERVATIONS');
  });
  it('load url with lastnperiods and custom time selection', async () => {
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb&lo=3&lom=LASTNPERIODS&pd=2017%2C2020`,
    );
    await page.goto(nextUrl);
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('only time selection should be kept', async () => {
    const url = await page.url();
    expect(url).toContain('pd=2017%2C2020');
    expect(url).not.toContain('lo=');
    expect(url).not.toContain('lom=');
  });
});
