import initHealthcheck from './healthcheck';
import initApi from './api';
import { initResources } from '../utils';

const ressources = [initHealthcheck, initApi];

module.exports = initResources(ressources);
