import React, { Fragment } from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(() => ({
  icon: {
    cursor: 'pointer',
    verticalAlign: 'bottom',
  },
}));

const Social = ({ links }) => {
  const classes = useStyles();
  return (
    <Fragment>
      {R.map(
        ({ Icon, id, link }) => (
          <IconButton
            key={id}
            component={Link}
            href={link}
            target="_blank"
            color="primary"
            rel="noopener noreferrer"
            size="small"
          >
            <Icon color="primary" fontSize="small" className={classes.icon} />
          </IconButton>
        ),
        links,
      )}
    </Fragment>
  );
};

Social.propTypes = {
  links: PropTypes.array,
};

export default Social;
