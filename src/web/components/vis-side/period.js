import { PeriodPicker } from '@sis-cc/dotstatsuite-visions';
import dateFns from 'date-fns';
import * as R from 'ramda';
import React from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { changeFrequencyPeriod } from '../../ducks/sdmx';
import { formatMessage } from '../../i18n';
import {
  getAvailableBoundaries,
  getAvailableFrequencies,
  getDatesBoundaries,
  getFrequency,
  getFrequencyArtefact,
  getPeriod,
} from '../../selectors/sdmx';
import { periodMessages } from '../messages';

const Period = () => {
  const intl = useIntl();
  const period = useSelector(getPeriod);
  const frequency = useSelector(getFrequency);
  const availableFrequencies = useSelector(getAvailableFrequencies);
  const boundaries = useSelector(getDatesBoundaries);
  const frequencyArtefact = useSelector(getFrequencyArtefact);
  const availableBoundaries = useSelector(getAvailableBoundaries);
  const dispatch = useDispatch();
  const changePeriod = period => dispatch(changeFrequencyPeriod({ valueId: frequency, period }));
  const changeFrequency = frequency =>
    dispatch(
      changeFrequencyPeriod({
        valueId: frequency,
        period: [dateFns.startOfYear(R.head(period)), dateFns.endOfYear(R.last(period))],
      }),
    );

  const labels = R.mergeRight(
    R.reduce(
      (memo, [id, message]) => R.assoc(id, formatMessage(intl)(message), memo),
      {},
      R.toPairs(periodMessages),
    ),
  )(availableFrequencies);

  return (
    <PeriodPicker
      period={period}
      availableFrequencies={R.keys(availableFrequencies)}
      availableBoundaries={availableBoundaries}
      boundaries={boundaries}
      isBlank={R.isNil(period)}
      labels={labels}
      frequencyDisabled={R.pipe(R.prop('display'), R.equals(false))(frequencyArtefact)}
      defaultFrequency={frequency}
      changePeriod={changePeriod}
      changeFrequency={changeFrequency}
    />
  );
};

export default Period;
