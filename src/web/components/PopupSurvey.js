import React, { useEffect } from 'react';
import { Button, Dialog, IconButton, Link, Paper, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage } from '../i18n';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  container: {
    padding: theme.spacing(1, 1, 0, 0),
    backgroundImage: ({ img }) => `url(${img})`,
    backgroundColor: '#0660EF',
    width: '700px',
    height: '550px',
  },
  text: {
    width: '65%',
    fontFamily: 'Noto Sans Display, sans-serif',
    color: '#FFFFFF',
    display: 'flex',
    flexDirection: 'column',
    padding: '120px 0px 30px 50px',
  },
  title: {
    fontSize: '47px',
    fontWeight: '900',
    lineHeight: 1,
  },
  button: {
    fontSize: '16px',
    backgroundColor: '#FFFFFF',
    color: '#024BCE',
    width: '290px',
    height: '43px',
    borderRadius: '35px',
    marginTop: '40px',
    fontWeight: 'bold',
  },
}));

const PopupSurvey = ({ url, isShowing, img }) => {
  const classes = useStyles({ img });
  const [openSurvey, setOpenSurvey] = React.useState(false);
  useEffect(() => {
    const hasSurveyBeenShownBefore = localStorage.getItem('hasSurveyBeenShown');
    if (!hasSurveyBeenShownBefore && isShowing) {
      setOpenSurvey(true);
      localStorage.setItem('hasSurveyBeenShown', true);
    }
  }, [isShowing]);
  const handleClose = () => {
    setOpenSurvey(false);
  };
  return (
    <Dialog
      open={openSurvey}
      maxWidth={'md'}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <Paper elevation={0} className={classes.container}>
        <IconButton style={{ float: 'right', color: 'white' }} size="small" onClick={handleClose}>
          <CloseIcon fontSize="small" />
        </IconButton>
        <Typography className={classes.text}>
          <span className={classes.title}>
            <FormattedMessage id="de.survey.title" />
          </span>
          <span style={{ fontSize: '30px', marginTop: '10px', fontWeight: '300' }}>
            {' '}
            <FormattedMessage id="de.survey.subtitle" />
          </span>
          <Link target="_blank" href={url}>
            <Button variant="contained" className={classes.button} onClick={handleClose}>
              {' '}
              <FormattedMessage id="de.survey.button" />
            </Button>
          </Link>
        </Typography>
      </Paper>
    </Dialog>
  );
};
PopupSurvey.propTypes = {
  isShowing: PropTypes.bool.isRequired,
  url: PropTypes.string,
  img: PropTypes.string,
};
export default PopupSurvey;
