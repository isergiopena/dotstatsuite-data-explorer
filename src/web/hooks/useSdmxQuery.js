import { useQuery } from 'react-query';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda';
import sdmxApi from '../api/sdmx';
import { getToken, getExtAuthOptions, getUser } from '../selectors/app';
import { getDatasource } from '../selectors/sdmx';
import { failedExtAuth } from '../ducks/app';
import { HTTP_AUTH_HEADER, withAuthorizationHeader } from '../api/sdmx/utils';
import { dataUrlIsTooLong } from '../ducks/sdmx';
import { getDataflow } from '../selectors/router';

const flattenObj = obj => {
  const go = obj_ =>
    R.chain(([k, v]) => {
      if (R.type(v) === 'Object' || R.type(v) === 'Array') {
        return R.map(([k_, v_]) => [`${k}.${k_}`, v_], go(v));
      } else {
        return [[k, v]];
      }
    }, R.toPairs(obj_));

  return R.fromPairs(go(obj));
};

const requestArgsToQueryKey = R.pipe(
  flattenObj,
  R.toPairs,
  R.map(R.join(':')),
  R.join(' | '),
);

export default (
  ctx,
  {
    successHandler = R.identity,
    errorHandler = R.identity,
    beforeHook = R.identity,
    isEnabled = true,
    queryKeyPrefix = '@@sdmx',
  } = {},
) => {
  const dispatch = useDispatch();

  // in CONFIG, datasources is an object
  // keys are datasource ids and id inside datasource object is a space id
  const { id: spaceId, hasExternalAuth = false } = useSelector(getDatasource);
  const dataflow = useSelector(getDataflow);
  const hasDataflow = !R.isEmpty(dataflow);

  const { credentials, isAnonymous } = useSelector(getExtAuthOptions(spaceId));
  const token = useSelector(getToken);
  const user = useSelector(getUser);

  const isExtAuthEnabled = !hasExternalAuth || isAnonymous || !!credentials;
  const isMasterEnabled = isEnabled && hasDataflow && isExtAuthEnabled;

  const requestArgs = withAuthorizationHeader({
    token,
    spaceId,
    credentials,
    isAnonymous,
  })(ctx.requestArgs);
  const authKey = R.has(HTTP_AUTH_HEADER, requestArgs.headers)
    ? `Authorized: ${user?.email}`
    : 'Not authorized';
  const queryKey = `${queryKeyPrefix}/${
    ctx.method
  } --> ${authKey} --> ${requestArgsToQueryKey(ctx.requestArgs)}`;
  const queryParams = {
    enabled: isMasterEnabled,
    onError: error => {
      if (error.code === 'E_JSON_PARSE' && R.isNil(spaceId)) {
        // redirect to invalid page with proper arg (ie invalid space)
        // when router will be full setup
      }
      errorHandler({ error, queryKey });
      if (error?.response?.status === 401 && hasExternalAuth)
        dispatch(failedExtAuth(spaceId));
    },
  };

  const tooLongRequestErrorCallback = () => {
    dispatch(dataUrlIsTooLong());
  };

  const { isLoading, data, error, isError, isSuccess } = useQuery(
    queryKey,
    () => {
      if (isMasterEnabled) beforeHook({ queryKey });
      return sdmxApi({
        method: ctx.method,
        requestArgs,
        tooLongRequestErrorCallback,
      });
    },
    queryParams,
  );

  // each time this hook is used the useEffect is settled
  // and dispatch is done too many times
  useEffect(() => {
    if (data && isSuccess && isMasterEnabled) {
      successHandler({ data, queryKey });
    }
  }, [data, isSuccess, isMasterEnabled]);

  return { data, isLoading, isError, error };
};
