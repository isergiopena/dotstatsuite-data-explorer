import * as L from '../';

describe('DE - lib - layout', () => {
  describe('getTableConfigLayout', () => {
    const layout = {
      header: [{ id: 'D0', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] }],
      sections: [{ id: 'D1', values: [{ id: 'v0' }, { id: 'v1' }] }],
      rows: [
        { id: 'D2', role: 'TIME_PERIOD', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] },
        {
          id: 'D3',
          values: [
            { id: 'v0' },
            { id: 'v1' },
            { id: 'v2' },
            { id: 'v3' },
            { id: 'v4' },
            { id: 'v5' },
          ],
        },
      ],
    };

    it('basic test', () => {
      expect(L.getTableConfigLayout(layout)).toEqual({
        header: [
          {
            id: 'D0',
            value: 'D0',
            isTimePeriod: false,
            values: [
              { id: 'D0-0', label: 'Xxxx' },
              { id: 'D0-1', label: 'Xxxx' },
              { id: 'D0-2', label: 'Xxxx' },
            ],
          },
        ],
        sections: [
          {
            id: 'D1',
            value: 'D1',
            isTimePeriod: false,
            values: [
              { id: 'D1-0', label: 'Xxxx' },
              { id: 'D1-1', label: 'Xxxx' },
            ],
          },
        ],
        rows: [
          {
            id: 'D2',
            value: 'D2',
            isTimePeriod: true,
            role: 'TIME_PERIOD',
            values: [
              { id: 'D2-0', label: 'Xxxx' },
              { id: 'D2-1', label: 'Xxxx' },
              { id: 'D2-2', label: 'Xxxx' },
            ],
          },
          {
            id: 'D3',
            value: 'D3',
            isTimePeriod: false,
            values: [
              { id: 'D3-0', label: 'Xxxx' },
              { id: 'D3-1', label: 'Xxxx' },
              { id: 'D3-2', label: 'Xxxx' },
            ],
          },
        ],
      });
    });
  });
});
