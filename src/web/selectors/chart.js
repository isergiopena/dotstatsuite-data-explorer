import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getDataFrequency, getTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules } from '@sis-cc/dotstatsuite-components';
import { getDisplay, getViewer } from './router';
import { getDataAttributes, getDataDimensions } from './data';
import { getData } from './sdmx';
import { getCustomAttributes, getVisChoroMap } from '.';
import * as Settings from '../lib/settings';

const getChart = R.prop('chart');
export const getChartConfigState = createSelector(getChart, R.identity);

export const getHasNeedOfResponsiveSize = createSelector(
  getChartConfigState,
  ({ responsiveWidth, responsiveHeight }) => R.isNil(responsiveWidth) || R.isNil(responsiveHeight),
);

export const getHasNeedOfComputedAxis = createSelector(
  getChartConfigState,
  R.pipe(
    R.pick([
      'computedMinX',
      'computedMaxX',
      'computedStepX',
      'computedPivotX',
      'computedMinY',
      'computedMaxY',
      'computedStepY',
      'computedPivotY',
    ]),
    R.all(R.isNil),
  ),
);

export const getFrequency = createSelector(
  getDataDimensions,
  getDataAttributes,
  (dimensions, attributes) => getDataFrequency({ dimensions, attributes }),
);

export const getFocused = createSelector(getChartConfigState, R.pick(['highlight', 'baseline']));

export const getChartDimension = createSelector(
  getData,
  getViewer,
  getChartConfigState,
  (data, type, config) => rules.toSingularity({ data, type }, config),
);

export const getFormaterIds = createSelector(
  getCustomAttributes,
  R.pipe(R.pick(['prefscale', 'decimals']), R.mapObjIndexed(R.of)),
);

export const getChartSeries = createSelector(
  getData,
  getViewer,
  getFocused,
  getChartDimension,
  getVisChoroMap,
  getDisplay,
  getFormaterIds,
  (data, type, focus, chartDimension, map, display, formaterIds) => {
    if (R.isNil(data)) {
      return null;
    }
    return rules.series(data, type, focus, chartDimension, map, display, formaterIds);
  },
);

export const getChartOptions = createSelector(
  getViewer,
  getVisChoroMap,
  getChartConfigState,
  (type, map, config) =>
    rules.toChartOptions({ type, map, options: Settings.chartOptions }, config),
);

export const getChartData = createSelector(
  getChartSeries,
  getFrequency,
  getFocused,
  getChartDimension,
  (series, frequency, focused, chartDimension) => ({
    frequency,
    series,
    share: { focused, chartDimension },
  }),
);

export const getHasNoTime = createSelector(getData, getViewer, (data, type) => {
  if (!R.equals('TimelineChart', type)) {
    return false;
  }
  const dimensions = R.pathOr([], ['structure', 'dimensions', 'observation'], data);
  const timeDimension = getTimePeriodDimension({ dimensions });
  if (R.isNil(timeDimension)) {
    return true;
  }
  return R.pipe(R.propOr([], 'values'), R.length, R.gte(1))(timeDimension);
});
