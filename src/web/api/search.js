import * as R from 'ramda';
import axios from 'axios';
import { configParser, searchParser } from '../lib/search';
import { datasources } from '../lib/settings';

let globalConfig = {};
/* eslint-disable-line no-shadow */
const setConfig = config => (globalConfig = { ...globalConfig, ...config });

const endpoint = (path, config = globalConfig) => `${config.endpoint}${path}`;

const _get = (path, parser) => ({ requestArgs, parserArgs }) => {
  const tenant = window?.CONFIG?.member?.id;
  if (R.isNil(tenant)) throw new Error('No tenant');
  if (R.isEmpty(datasources)) throw new Error('No datasource in the scope');

  const prepareRequestArgs = R.over(
    R.lensPath(['facets', 'datasourceId']),
    R.ifElse(R.either(R.isNil, R.isEmpty), R.always(R.keys(datasources)), R.identity),
  );

  return axios
    .post(endpoint(path), prepareRequestArgs(requestArgs), { params: { tenant } })
    .then(R.pipe(R.prop('data'), parser(parserArgs)));
};

const methods = {
  setConfig,
  getConfig: _get('/search', configParser),
  getSearch: _get('/search', searchParser),
};

const error = method => () => {
  throw new Error(`Unkown method: ${method}`);
};

const main = ({ method, ...rest }) => (methods[method] || error(method))(rest);
R.compose(
  R.forEach(([name, fn]) => (main[name] = fn)),
  R.toPairs,
)(methods);

export default main;
