import { all } from 'redux-saga/effects';
import { watchRequestStructure, watchParseStructure } from './sdmx/structure';
import { watchParseData, watchRequestDataFile } from './sdmx/data';
import { watchRequestMetadata } from './sdmx/metadata';
import { chartSaga } from './chart';
import { watchRequestAllHCL } from './sdmx/hierarchical-codelists';

export default function* rootSaga() {
  yield all([
    watchRequestStructure(),
    watchParseStructure(),
    watchRequestDataFile(),
    watchParseData(),
    chartSaga(),
    watchRequestMetadata(),
    watchRequestAllHCL(),
  ]);
}
