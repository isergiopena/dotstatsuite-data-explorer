import highlightParser from '../highlightParser';

describe('highlightParser', () => {
  it('should handle flat case', () => {
    expect(
      highlightParser('<mark>industrial</mark> machinery and equipment#C332#'),
    ).toEqual('<mark>industrial</mark> machinery and equipment');
  });
  it('should handle truncated flat case', () => {
    expect(
      highlightParser('<mark>industrial</mark> machinery and equipment'),
    ).toEqual('<mark>industrial</mark> machinery and equipment');
  });
  it('should handle truncated flat case', () => {
    expect(
      highlightParser('machinery and equipment#<mark>industrial</mark>#'),
    ).toEqual('');
  });
  it('should handle tree case', () => {
    expect(
      highlightParser(
        '0|Percentage of employment in <mark>industry</mark>#EMP_PERC_IND#',
      ),
    ).toEqual('Percentage of employment in <mark>industry</mark>');
  });
  it('should handle deep tree case', () => {
    expect(
      highlightParser('3|Y4lla#Y1#|Y5lla#Y2#|Y6 <mark>lla</mark> yalla#Y3#'),
    ).toEqual('Y4lla > Y5lla > Y6 <mark>lla</mark> yalla');
  });
  it('should handle truncated tree case', () => {
    expect(
      highlightParser('3|Y4lla#Y1#|Y5lla#Y2#|Y6 <mark>lla</mark>'),
    ).toEqual('Y4lla > Y5lla > Y6 <mark>lla</mark>');
  });
});
