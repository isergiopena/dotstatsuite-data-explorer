import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import { useSelector } from 'react-redux';
import Divider from '@material-ui/core/Divider';
import MuiButton from '@material-ui/core/Button';
import { getExternalResources } from '../../../selectors/search';
import { FormattedMessage } from '../../../i18n';
import { Button, Menu } from '../../visions/DeToolBar/helpers';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
  link: {
    margin: 0,
    padding: theme.spacing(0.75, 2),
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'end',
  },
  linkIcon: {
    height: 20,
    paddingRight: theme.spacing(1),
    alignItems: 'center',
  },
}));

const Component = ({
  dataflowId,
  downloads,
  isDownloading,
  isExternalLoading,
  action,
  externalMessage,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const externalResources = useSelector(getExternalResources);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
    if (R.not(R.has(dataflowId, externalResources))) {
      action();
    }
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const resources = R.propOr([], dataflowId)(externalResources);

  return (
    <React.Fragment>
      <Button
        startIcon={<GetAppIcon />}
        selected={Boolean(anchorEl)}
        loading={isDownloading}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.action.download" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(
          ({ id, label, link, callback, filename }) => (
            <MuiButton
              key={id}
              component="a"
              color="primary"
              href={link}
              onClick={R.is(Function, callback) ? callback : null}
              className={classes.link}
              id={id}
              tabIndex={0}
              download={filename}
            >
              {label}
            </MuiButton>
          ),
          downloads,
        )}
        <Divider />
        {isExternalLoading && (
          <ListItemText primaryTypographyProps={{ color: 'primary', align: 'center' }}>
            <CircularProgress size={24} />
          </ListItemText>
        )}
        {R.and(R.not(isExternalLoading), R.isEmpty(resources)) && (
          <MenuItem dense disabled>
            <ListItemText primaryTypographyProps={{ color: 'primary' }}>
              <span>{externalMessage}</span>
            </ListItemText>
          </MenuItem>
        )}
        {R.not(R.isEmpty(resources)) &&
          R.map(
            ({ id, label, link, img }) => (
              <MuiButton
                key={id}
                component="a"
                color="primary"
                href={link}
                className={classes.link}
                id={id}
                tabIndex={0}
                download
              >
                {R.not(R.isNil(img)) && (
                  <img src={img} className={classes.linkIcon} alt={`${id} icon`} />
                )}
                {label}
              </MuiButton>
            ),
            resources,
          )}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  downloads: PropTypes.array.isRequired,
  isDownloading: PropTypes.bool,
  isExternalLoading: PropTypes.bool,
  action: PropTypes.func,
  dataflowId: PropTypes.string,
  externalMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

export default Component;
