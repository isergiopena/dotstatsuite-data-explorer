import { defineMessages, useIntl } from 'react-intl';
import { useSelector } from 'react-redux';
import * as R from 'ramda';
import { formatMessage } from '../i18n';
import { getAsset, search } from '../lib/settings';
import { getVisDataflow, getVisDimensionFormat } from '../selectors';
import { getDefaultTitleLabel } from '../selectors/data';
import { getDataflow, getLocale } from '../selectors/router';
import {
  getActualConstraints,
  getDataflowDescription,
  getDimensions,
  getExternalResources,
  getHierarchySchemes,
  getObservationsCount,
  getValidFrom,
} from '../selectors/sdmx';
import {
  groupAttributes,
  groupDimensions,
  isLast,
} from '../components/vis/overview/utils';
import useSdmxBlankData from './useSdmxBlankData';
import { makeSelectedHierarchySchemesList } from '../utils/structured-hierarchies-overview';
import { getVisUrl } from '../utils/router';
import { useRelatedDataflows } from './useRelatedDataflows';

const messages = defineMessages({
  relatedFiles: { id: 'de.related.files' },
  observationsCount: { id: 'de.observations.count' },
  dataSource: { id: 'de.data.source' },
  validFrom: { id: 'de.last.updated' },
  complementaryData: { id: 'de.complementary.data' },
});

export default () => {
  const intl = useIntl();
  const locale = useSelector(getLocale);
  const labelAccessor = useSelector(
    getVisDimensionFormat({ id: 'id', name: 'label' }),
  );
  const nameAccessor = useSelector(getVisDimensionFormat());
  const dataflow = useSelector(getVisDataflow);
  const { datasourceId } = useSelector(getDataflow);
  const dataflowDescription = useSelector(getDataflowDescription);
  const dimensions = useSelector(getDimensions);
  const {
    attributes,
    observationsCount: blankObservationsCount,
  } = useSdmxBlankData();
  const label = useSelector(getDefaultTitleLabel);
  const hierarchySchemes = useSelector(getHierarchySchemes);
  const actualConstraints = useSelector(getActualConstraints);
  const externalResources = useSelector(getExternalResources);
  const defaultObservationsCount = useSelector(getObservationsCount);
  const validFrom = useSelector(getValidFrom);
  const indexedRelatedDataflows = useRelatedDataflows();

  const observationsCount = R.defaultTo(
    defaultObservationsCount,
    blankObservationsCount,
  );

  const footerProps = {
    isSticky: true,
    source: {
      label,
      link: window.location.href,
    },
    logo: getAsset('viewerFooter', locale),
  };
  const homeFacetIds = new Set(R.propOr([], 'homeFacetIds', search));
  const { oneDimensions, manyDimensions } = groupDimensions(
    dimensions,
    actualConstraints,
  );
  const oneAttributes = R.pipe(groupAttributes, R.propOr([], 'oneAttributes'))(
    attributes || [],
    actualConstraints,
    oneDimensions,
  );

  const dataSpaceLabel =
    window.CONFIG?.member?.scope?.spaces?.[datasourceId]?.label ||
    window.CONFIG?.member?.scope?.datasources?.[datasourceId]?.label;

  const lists = {
    dataSource: [
      {
        id: 'dataSource',
        name: formatMessage(intl)(messages.dataSource),
        values: [{ name: dataSpaceLabel }],
      },
    ],
    observationsCount: [
      {
        id: 'observationsCount',
        name: formatMessage(intl)(messages.observationsCount),
        values: [{ name: observationsCount }],
      },
    ],
    validFrom: [
      {
        id: 'validFrom',
        name: formatMessage(intl)(messages.validFrom),
        values: [
          {
            name: intl.formatDate(validFrom, {
              year: 'numeric',
              month: 'long',
              day: '2-digit',
              hour: 'numeric',
              minute: 'numeric',
              second: 'numeric',
            }),
          },
        ],
      },
    ],
    relatedFiles: [
      {
        id: 'relatedFiles',
        name: formatMessage(intl)(messages.relatedFiles),
      },
    ],
    complementaryData: [
      {
        id: 'ComplementaryData',
        name: formatMessage(intl)(messages.complementaryData),
      },
    ],
  };
  const selectedHierarchySchemes = R.reduce((acc, schemes) => {
    if (homeFacetIds.has(R.head(schemes)?.name)) {
      const head = R.head(schemes);
      return R.append({ ...head, values: R.tail(schemes) }, acc);
    }
    return acc;
  }, [])(hierarchySchemes);
  const makeHierarchyProps = (index, hierarchies = []) => {
    return {
      list: [hierarchies],
      accessor: nameAccessor,
      hasGroupSeparator: !isLast(
        index,
        makeSelectedHierarchySchemesList(selectedHierarchySchemes),
      ),
    };
  };
  const getSortedRelatedDataflows = dfs =>
    R.sortBy(R.pipe(R.prop('label'), R.replace(/ /g, ''), R.toLower))(dfs);

  const complementaryData = !R.isEmpty(indexedRelatedDataflows)
    ? R.map(dataflow => ({
        ...dataflow,
        dataflowId: dataflow.code,
        url: getVisUrl(
          {},
          { ...dataflow, dataflowId: dataflow.code, datasourceId },
        ),
      }))(getSortedRelatedDataflows(indexedRelatedDataflows))
    : [];

  return {
    title: nameAccessor(dataflow),
    labelAccessor,
    selectedHierarchySchemes: makeSelectedHierarchySchemesList(
      selectedHierarchySchemes,
    ),
    externalResources,
    dataflowDescription,
    observationsCount,
    dataSpaceLabel,
    validFrom,
    footerProps,
    oneDimensions,
    manyDimensions,
    oneAttributes,
    homeFacetIds,
    lists,
    complementaryData,
    makeHierarchyProps,
  };
};
