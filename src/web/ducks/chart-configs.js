const initialState = {
  highlight: [],
  baseline: [],
  fixedWidth: undefined,
  fixedHeight: undefined,
  freqStep: undefined,
  responsiveWidth: undefined,
  responsiveHeight: undefined,
  scatterDimension: undefined,
  scatterX: undefined,
  scatterY: undefined,
  symbolDimension: undefined,
  stackedDimension: undefined,
  stackedMode: undefined,
  title: undefined,
  subtitle: undefined,
  sourceLabel: undefined,
  withLogo: true,
  withCopyright: true,
};

export const UPDATE_ATTEMPT_CHART_CONFIG = '@chart/UPDATE_ATTEMPT_CHART_CONFIG';
export const UPDATE_CHART_CONFIG = '@chart/UPDATE_CHART_CONFIG';

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case UPDATE_CHART_CONFIG:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
