import { createSelector } from 'reselect';
import * as R from 'ramda';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import Set from 'es6-set';
import {
  sdmxFormat,
  getFrequencies,
  parseDateFromSdmxPeriod,
  getFrequencyFromSdmxPeriod,
  getAjustedDate,
  getSdmxPeriod,
  getSubstructedDate,
  getAscendentDates,
} from '../lib/sdmx/frequency';
import { getDateInTheRange } from '../utils/date';
import {
  cellsLimit,
  getDatasource as getSettingsDatasource,
  locales,
  sdmxPeriodBoundaries,
} from '../lib/settings';
import {
  getLastNObservations,
  getLocale,
  getPeriod as getRouterPeriod,
  getHasDataAvailability,
  getDataquery,
  getDataflow,
  getDisplay,
  getLastNMode,
} from './router';
import {
  dataFileRequestArgsWrapper,
  getOnlyHasDataDimensions,
  rawDataRequestArgsWrapper,
} from '../lib/sdmx';
import { applyAutomatedSelection } from '../lib/sdmx/selection';
import { cleanDeadBranches } from '../utils/clean-dead-branches';
import { getMultiHierarchicalFilters } from '../lib/sdmx/hierarchical-codelist';
import * as filtersLib from '../lib/sdmx/filters';
import { LASTNOBSERVATIONS, LASTNPERIODS } from '../utils/used-filter';
//------------------------------------------------------------------------------------------------#0
const getSdmx = R.prop('sdmx');
const getMicrodataDimensionId = createSelector(
  R.prop('microdata'),
  R.prop('dimensionId'),
);

//------------------------------------------------------------------------------------------------#1
export const getAnnotations = createSelector(
  getSdmx,
  R.pathOr([], ['data', 'structure', 'annotations']),
);
export const getDimensions = createSelector(
  getSdmx,
  R.propOr([], 'dimensions'),
);
export const getAttributes = createSelector(
  getSdmx,
  R.propOr([], 'attributes'),
);
export const getData = createSelector(getSdmx, R.prop('data'));
export const getDataRange = createSelector(getSdmx, R.prop('range'));
export const getTimePeriodArtefact = createSelector(
  getSdmx,
  R.prop('timePeriodArtefact'),
);
export const getExternalResources = createSelector(
  getSdmx,
  R.prop('externalResources'),
);
export const getExternalReference = createSelector(
  getSdmx,
  R.prop('externalReference'),
);
export const getDataRequestSize = createSelector(
  getSdmx,
  R.prop('dataRequestSize'),
);
export const getConstraints = createSelector(getSdmx, R.prop('constraints'));
export const getHierarchySchemes = createSelector(
  getSdmx,
  R.propOr([], 'hierarchySchemes'),
);
export const getObservationsCount = createSelector(
  getSdmx,
  R.prop('observationsCount'),
);
export const getObservationsType = createSelector(
  getSdmx,
  R.prop('observationsType'),
);
export const getDataflowDescription = createSelector(
  getSdmx,
  R.prop('description'),
);
export const getDataflowName = createSelector(getSdmx, ({ name, data }) =>
  R.isNil(data) ? name : R.path(['structure', 'name'], data),
);
export const getHierarchies = createSelector(getSdmx, R.prop('hierarchies'));
export const getDefaultTimePeriod = createSelector(
  getSdmx,
  R.prop('defaultPeriod'),
);
export const getHiddenValuesAnnotations = createSelector(
  getSdmx,
  R.propOr({}, 'hiddenValuesAnnotation'),
);
export const getTextAlign = createSelector(getSdmx, R.prop('textAlign'));
export const getAutomatedSelections = createSelector(
  getSdmx,
  R.prop('automatedSelections'),
);
//------------------------------------------------------------------------------------------------#2
// available constraints come from availableContraints request  theses contraints change relatively to the selection.
export const getAvailableConstraints = createSelector(
  getConstraints,
  R.prop('available'),
);
export const getAvailableConstraintsWithoutPeriodFetched = createSelector(
  getConstraints,
  R.prop('availableWithoutPeriodFetched'),
);
// actual contraints come from structure request theses contraints are fixed.
export const getActualConstraints = createSelector(
  getConstraints,
  R.prop('actual'),
);
export const getValidFrom = createSelector(getConstraints, R.prop('validFrom'));
export const getDataPoints = createSelector(
  getConstraints,
  R.prop('dataPoints'),
);
export const getFrequencyArtefact = createSelector(
  getDimensions,
  getAttributes,
  (dimensions = [], attributes = []) =>
    SDMXJS.getFrequencyArtefact({ dimensions, attributes }),
);

export const getDatasource = createSelector(
  getDataflow,
  getExternalReference,
  (dataflow, externalReference) =>
    R.defaultTo(
      {},
      R.isNil(externalReference)
        ? getSettingsDatasource(R.prop('datasourceId', dataflow))
        : externalReference.datasource,
    ),
);

export const getRawStructureRequestArgs = createSelector(
  getDataflow,
  getLocale,
  getExternalReference,
  (dataflow, locale, externalReference) =>
    R.isNil(externalReference)
      ? {
          datasource: getSettingsDatasource(R.prop('datasourceId', dataflow)),
          identifiers: {
            code: R.prop('dataflowId', dataflow),
            ...R.pick(['agencyId', 'version'], dataflow),
          },
          locale,
        }
      : R.assoc('locale', locale, externalReference),
);
//------------------------------------------------------------------------------------------------#3
export const getFrequency = createSelector(
  getRouterPeriod,
  getFrequencyArtefact,
  getDataquery,
  (sdmxPeriod, frequencyArtefact, dataquery) =>
    R.pipe(
      R.when(R.isNil, R.always('')),
      R.split('.'),
      R.view(R.lensIndex(R.prop('index')(frequencyArtefact))),
      R.ifElse(
        R.either(R.isEmpty, R.isNil),
        R.always(getFrequencyFromSdmxPeriod(sdmxPeriod)),
        R.identity,
      ),
    )(dataquery),
);

export const getStructureRequestArgs = createSelector(
  getRawStructureRequestArgs,
  args =>
    SDMXJS.getRequestArgs({
      ...args,
      type: 'dataflow',
      withPartialReferences: true,
    }),
);

export const getHierarchisedDimensions = createSelector(
  getDimensions,
  getHierarchies,
  (dimensions, hierarchies) =>
    getMultiHierarchicalFilters(dimensions, hierarchies),
);

export const getDimensionsWithDataQuerySelection = createSelector(
  getHierarchisedDimensions,
  getDataquery,
  getMicrodataDimensionId,
  getAutomatedSelections,
  (filters, dataquery, microdataDimId, automatedSelections) =>
    R.pipe(
      R.useWith(
        (filters, dataquery) =>
          R.addIndex(R.map)((_filter, index) => {
            if (R.isEmpty(_filter)) return _filter;
            const filter = R.has(_filter.id, automatedSelections)
              ? R.over(R.lensProp('values'), values =>
                  applyAutomatedSelection(
                    values,
                    R.prop(_filter.id, automatedSelections),
                  ),
                )(_filter)
              : _filter;
            if (
              R.pipe(R.nth(index), R.anyPass([R.isEmpty, R.isNil]))(dataquery)
            )
              return filter;
            const valueIdsSet = new Set(
              R.pipe(R.nth(index), R.split('+'))(dataquery),
            );
            return R.over(
              R.lensProp('values'),
              R.map(
                R.ifElse(
                  ({ id }) => valueIdsSet.has(id),
                  R.assoc('isSelected', true),
                  R.identity,
                ),
              ),
              filter,
            );
          }, filters),
        [R.identity, R.ifElse(R.isNil, R.always([]), R.split('.'))],
      ),
      R.reject(
        R.anyPass([
          R.pipe(R.prop('display'), R.not),
          R.propEq('id', microdataDimId),
          SDMXJS.isFrequencyDimension,
          R.pipe(R.prop('values'), R.length, R.gte(1)),
        ]),
      ),
    )(filters, dataquery),
);

//------------------------------------------------------------------------------------------------#4

export const getFilters = createSelector(
  getHasDataAvailability,
  getDimensionsWithDataQuerySelection,
  getAvailableConstraints,
  getHiddenValuesAnnotations,
  (hasDataAvailability, filters, availableConstraints, hiddenIds) =>
    R.pipe(
      dimensions =>
        SDMXJS.constrainDimensions(
          dimensions,
          availableConstraints,
          'isEnabled',
        ),
      R.when(
        R.always(hasDataAvailability),
        R.pipe(R.map(R.over(R.lensProp('values'), cleanDeadBranches))),
      ),
      R.filter(filter =>
        R.pipe(
          R.propOr([], 'values'),
          values =>
            R.length(values) > 1 &&
            (hiddenIds[filter.id]
              ? !R.isEmpty(
                  R.difference(R.pluck('id')(values), hiddenIds[filter.id]),
                )
              : true),
        )(filter),
      ),
    )(filters),
);

/*
  Previous getSelection selector was unneccessary depending on getFilters.
  Filters are listening to data requests in order to update data availability but used filters don't,
  just need to inherit dimensions and hierarchies definitions provided by the structure and listening to dataquery updates.

  Multi hierarchy still needs to be handled for display, and props definition in component should be reviewed and enhanced.
*/
export const getUsedFilters = createSelector(
  getDimensionsWithDataQuerySelection,
  dims => filtersLib.getUsedFilters(dims),
);

export const getAvailableFrequencies = createSelector(
  getFrequencyArtefact,
  getHasDataAvailability,
  (frequencyArtefact = {}, hasDataAvailability) => {
    if (R.propEq('display', false)(frequencyArtefact)) return [];

    return R.pipe(
      R.ifElse(
        R.always(hasDataAvailability),
        R.pipe(R.of, getOnlyHasDataDimensions, R.head),
        R.identity,
      ),
      getFrequencies,
    )(frequencyArtefact);
  },
);

export const getDatesBoundaries = createSelector(
  getTimePeriodArtefact,
  getFrequency,
  getHasDataAvailability,
  (artefact, frequency, hasDataAvailability) => {
    if (R.isNil(artefact)) {
      return null;
    }
    const boundaries = hasDataAvailability
      ? R.propOr([], 'timePeriodBoundaries', artefact)
      : sdmxPeriodBoundaries;
    return R.map(getAjustedDate(frequency))(boundaries);
  },
);

//------------------------------------------------------------------------------------------------#5
export const getPeriod = createSelector(
  getRouterPeriod,
  getFrequency,
  getDatesBoundaries,
  (sdmxPeriod, frequency, boundaries) => {
    if (R.isNil(sdmxPeriod)) return [undefined, undefined];
    const period = parseDateFromSdmxPeriod(frequency, sdmxPeriod);
    return R.ifElse(
      R.pipe(R.length, R.equals(2)),
      R.map(getDateInTheRange(boundaries)),
      R.always(R.map(getDateInTheRange(boundaries))(sdmxPeriod)),
    )(period);
  },
);

export const getTimeFormats = createSelector(getLocale, locale => {
  return R.mergeRight(sdmxFormat, {
    M: R.pathOr('YYYY MMM', [locale, 'timeFormat'], locales),
  });
});

export const getActualBoundaries = createSelector(
  getActualConstraints,
  R.pathOr([], ['TIME_PERIOD', 'boundaries']),
);

export const getAvailableBoundaries = createSelector(
  getAvailableConstraints,
  R.pathOr([], ['TIME_PERIOD', 'boundaries']),
);
//------------------------------------------------------------------------------------------------#6
//TODO
export const getDataRequestParams = createSelector(
  getRouterPeriod,
  getLastNObservations,
  getAvailableBoundaries,
  getLastNMode,
  getFrequency,
  (_period, lastNObservations, availablePeriods = [], lastNMode, frequency) => {
    const period = R.isNil(_period) ? [undefined, undefined] : _period;

    const formattedAvailablePeriod = R.ifElse(
      R.either(R.isNil, R.isEmpty),
      R.always([undefined, undefined]),
      R.map(period => new Date(period.toString())),
    )(availablePeriods);

    const diff =
      !R.isNil(R.last(formattedAvailablePeriod)) && !R.isNil(lastNObservations)
        ? getSubstructedDate(
            R.last(formattedAvailablePeriod),
            lastNObservations,
            frequency,
          )
        : null;

    const newStartPeriod =
      !R.isNil(diff) && R.isNil(R.head(period))
        ? R.ifElse(
            R.either(R.isNil, R.isEmpty),
            R.always([undefined, undefined]),
            R.identity,
          )(getSdmxPeriod(frequency))(
            getAscendentDates(diff, R.head(formattedAvailablePeriod)),
          )
        : R.head(period);

    return {
      startPeriod: R.equals(lastNMode, LASTNPERIODS)
        ? newStartPeriod
        : R.head(period),
      endPeriod: R.last(period),
      lastNObservations: R.equals(lastNMode, LASTNOBSERVATIONS)
        ? lastNObservations
        : null,
    };
  },
);

export const getDataRequestRange = createSelector(getDataRequestSize, size => {
  const _size = R.isNil(size) || size <= 0 ? cellsLimit : size;
  if (!_size) {
    return null;
  }
  return _size;
});

export const getRefinedDataRange = createSelector(
  getData,
  getDataRange,
  (data, range) => {
    if (!R.isNil(range) && !R.isEmpty(range)) {
      return range;
    }
    const observations = R.pathOr({}, ['dataSets', 0, 'observations'], data);
    const count = R.length(R.values(observations));
    return { count, total: count };
  },
);

export const getRawDataRequestArgs = createSelector(
  getRawStructureRequestArgs,
  getDataquery,
  getDataRequestParams,
  getExternalReference,
  getDataRequestRange,
  rawDataRequestArgsWrapper,
);

export const getDataRequestArgs = createSelector(
  getRawDataRequestArgs,
  ({ datasource, ...rest }) => {
    return R.pipe(
      SDMXJS.getRequestArgs,
      R.assoc('datasourceId', R.prop('id', datasource)),
    )({ ...rest, datasource, type: 'data' });
  },
);

export const getIsDataUrlTooLong = createSelector(
  getSdmx,
  R.prop('isDataUrlTooLong'),
);
export const getIsApiQueryCopied = createSelector(
  getSdmx,
  R.prop('isApiQueryCopied'),
);
export const getDataFileRequestArgs = isFull =>
  createSelector(
    getRawDataRequestArgs,
    getDisplay,
    dataFileRequestArgsWrapper(isFull),
  );

export const getDataSourceHeaders = createSelector(
  getDataRequestArgs,
  R.prop('headers'),
);

export const getStructureUrl = createSelector(
  getRawStructureRequestArgs,
  getExternalReference,
  (args, externalReference) =>
    SDMXJS.getSDMXUrl({
      ...R.when(
        R.always(R.not(R.isNil(externalReference))),
        R.mergeLeft(externalReference),
      )(args),
      type: 'dataflow',
    }),
);

export const getDataUrl = ({ agnostic }) =>
  createSelector(getRawDataRequestArgs, args =>
    SDMXJS.getSDMXUrl({ ...args, agnostic, type: 'data' }),
  );

export const getAvailableConstraintsArgs = createSelector(
  getRawDataRequestArgs,
  args =>
    R.pipe(
      R.over(R.lensProp('params'), params => {
        if (R.isNil(params)) return params;
        return R.pick(['endPeriod', 'startPeriod'], params);
      }),
      SDMXJS.getRequestArgs,
      R.over(R.lensProp('params'), R.assoc('mode', 'available')),
      R.assoc(
        'supportsPostLongRequests',
        R.propOr(false, 'supportsPostLongRequests', args.datasource),
      ),
      R.assoc('datasourceId', R.path(['datasource', 'id'], args)),
    )({
      ...args,
      withReferences: false,
      type: 'availableconstraint',
    }),
);

export const getIsCsvFileLinkHandled = createSelector(
  getDatasource,
  R.propOr(true, 'supportsCsvFile'),
);
