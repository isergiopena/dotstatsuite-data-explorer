import React, { useLayoutEffect, useRef, useState } from 'react';
import { useIntl } from 'react-intl';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Collapse from '@material-ui/core/Collapse';
import { Grid, IconButton, makeStyles } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import cx from 'classnames';
import * as R from 'ramda';
import { Tag } from '@sis-cc/dotstatsuite-visions';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { useSelector } from 'react-redux';
import { getUsedFilters } from '../selectors/sdmx';
import { FormattedMessage, formatMessage } from '../i18n';
import {
  MARGE_RATIO,
  MARGE_SIZE,
  SIDE_WIDTH,
  SMALL_SIDE_WIDTH,
} from '../utils/constants';
import { getIsFull } from '../selectors';
import { getIsMicrodata } from '../selectors/microdata';
import AppliedFilters from './vis-side/applied-filters';
import { ClearFilters } from './vis-side/used-filter';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  usedFilterslabel: {
    paddingRight: 5,
    fontFamily: 'Roboto Slab, serif',
    fontSize: '17px',
    color: 'Body !important',
  },
  expandIcon: {
    color: theme.palette.primary.main,
  },
  toolbar: {
    marginTop: ({ isNarrow, isFull }) => (isFull || isNarrow ? 0 : -25),
    position: 'sticky',
    [theme.breakpoints.down('xs')]: {
      maxWidth: 'none',
    },
    [theme.breakpoints.between('sm', 'md')]: {
      ['left']: `${MARGE_SIZE}%`,
      maxWidth: ({ visWidth }) => visWidth,
    },
    [theme.breakpoints.only('md')]: {
      ['left']: ({ isFull }) =>
        isFull
          ? `${MARGE_SIZE}%`
          : `calc(${MARGE_SIZE}% + ${SMALL_SIDE_WIDTH + theme.spacing(2)}px)`,
      maxWidth: ({ visWidth, isFull }) =>
        visWidth - (isFull ? 0 : SMALL_SIDE_WIDTH) - theme.spacing(2),
    },
    [theme.breakpoints.up('lg')]: {
      ['left']: ({ isFull }) =>
        isFull
          ? `${MARGE_SIZE}%`
          : `calc(${MARGE_SIZE}% + ${SIDE_WIDTH + theme.spacing(2)}px)`,
      maxWidth: ({ visWidth, isFull }) =>
        visWidth - (isFull ? 0 : SIDE_WIDTH) - theme.spacing(2),
    },
  },
  title: {
    width: 'auto',
    flexShrink: 0,
    paddingRight: '5px',
  },
}));
const UsedFilters = ({
  counter,
  maxWidth,
  data,
  labels = {},
  onDeleteAll,
  clearAllLabel,
  dataCount,
  dataCountLabel,
  dataCountId,
}) => {
  const intl = useIntl();
  const isMicrodata = useSelector(getIsMicrodata);
  const isMD = useMediaQuery(theme => theme.breakpoints.only('md'));
  const isNarrow = useMediaQuery(theme => theme.breakpoints.down('sm'));
  const isFull = useSelector(getIsFull());
  const classes = useStyles({
    visWidth: maxWidth ? maxWidth - maxWidth * (1 - MARGE_RATIO) : '100%',
    isFull: R.or(isFull, isMicrodata),
    isNarrow,
  });
  const collapsedSize = 29 * 2.33;
  const [open, setOpen] = React.useState(false);
  const selection = useSelector(getUsedFilters);
  const [showExpandIcon, setShowExpandIcon] = useState(false);
  const filtersRef = useRef(null);
  const handleClick = () => setOpen(!open);

  useLayoutEffect(() => {
    function checkFiltersHeight() {
      const filtersElement = filtersRef.current;
      if (!filtersElement) return;
      const lineHeight = parseInt(
        getComputedStyle(filtersElement).getPropertyValue('line-height'),
      );
      const filtersHeight = filtersElement.scrollHeight;
      const numLines = Math.round(filtersHeight / lineHeight);
      setShowExpandIcon(numLines > 2);
    }
    checkFiltersHeight();

    window.addEventListener('resize', checkFiltersHeight);

    return () => {
      window.removeEventListener('resize', checkFiltersHeight);
    };
  }, [filtersRef.current, selection]);

  return (
    <Grid
      container
      direction={isNarrow || isMD ? 'column' : 'row'}
      justifyContent="flex-start"
      wrap="nowrap"
      className={cx(classes.usedFilters, classes.toolbar)}
      tabIndex={0}
      aria-label={formatMessage(intl)(messages.titleLabel, { counter })}
    >
      <Grid
        item
        container
        direction={isNarrow || isMD ? 'row' : 'column'}
        justifyContent={isNarrow || isMD ? 'flex-start' : 'space-between'}
        className={classes.title}
        style={{ marginRight: '10px' }}
      >
        <Grid
          item
          container
          justifyContent="flex-start"
          wrap="nowrap"
          className={classes.title}
        >
          {showExpandIcon && (
            <Grid item>
              <IconButton
                tabIndex={0}
                style={{ float: 'right' }}
                size="small"
                color="primary"
                onClick={handleClick}
                aria-expanded={open}
                aria-label={formatMessage(intl)(messages.next)}
              >
                {' '}
                {open ? (
                  <ExpandLess style={{ float: 'right' }} />
                ) : (
                  <ExpandMore />
                )}
              </IconButton>
            </Grid>
          )}

          <Typography
            noWrap
            variant="body2"
            className={classes.usedFilterslabel}
          >
            <FormattedMessage id="vx.filters.current.title" />
          </Typography>
          <Tag data-testid="applied_filters_counter">{counter}</Tag>
        </Grid>
        {!R.isNil(dataCount) && (
          <Grid item>
            <Typography
              variant="body2"
              color="textSecondary"
              tabIndex={0}
              style={{ paddingTop: 3 }}
              id={dataCountId}
            >
              {dataCountLabel}
            </Typography>
          </Grid>
        )}
      </Grid>

      <Grid item>
        <Collapse in={open} timeout="auto" collapsedSize={collapsedSize}>
          <Grid>
            <AppliedFilters ref={filtersRef} data={data} labels={labels} />
            <ClearFilters onDelete={onDeleteAll} label={clearAllLabel} />
          </Grid>
        </Collapse>
      </Grid>
    </Grid>
  );
};
UsedFilters.propTypes = {
  counter: PropTypes.number,
  maxWidth: PropTypes.number,
  data: PropTypes.array.isRequired,
  labels: PropTypes.object.isRequired,
  onDeleteAll: PropTypes.func.isRequired,
  clearAllLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  dataCount: PropTypes.number,
  dataCountLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  dataCountId: PropTypes.string,
};
export default UsedFilters;
