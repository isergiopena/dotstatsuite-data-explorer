import { useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda';
import { getDimensions } from '../selectors/sdmx';
import { getMicrodataRequestArgs } from '../selectors/microdata';
import { FLUSH_MICRODATA, PARSE_MICRODATA } from '../ducks/microdata';
import { MICRODATA } from '../utils/constants';
import useSdmxQuery from './useSdmxQuery';

export default () => {
  const dispatch = useDispatch();

  const dimensions = useSelector(getDimensions);
  const requestArgs = useSelector(getMicrodataRequestArgs);

  const ctx = { method: 'getMicrodata', requestArgs };

  const { isLoading, isError, error } = useSdmxQuery(ctx, {
    beforeHook: () => dispatch({ type: FLUSH_MICRODATA }),
    successHandler: ({ data, queryKey }) =>
      dispatch({
        type: PARSE_MICRODATA,
        datatype: MICRODATA,
        data,
        queryKey,
      }),
    isEnabled: !R.all(R.isEmpty)(dimensions),
  });

  return { isLoading, isError, error };
};
