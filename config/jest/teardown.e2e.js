const os = require('os');
const rimraf = require('rimraf');
const path = require('path');

const DIR = path.join(os.tmpdir(), 'jest_puppeteer_global_setup');
module.exports = async function() {
  //await global._BROWSER_.close();
  await global._SFS_.close();
  await global._CONFIG_.close();
  await global._NSI_.close();
  rimraf.sync(DIR);
};
