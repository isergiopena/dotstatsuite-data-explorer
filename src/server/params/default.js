import path from 'path';
import { getSMTPEnv } from '../../lib/mailer';

const config = {
  appId: 'data-explorer',
  env: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  gitHash: process.env.GIT_HASH || 'local',
  publicPath: path.join(__dirname, '../../../public'),
  buildPath: path.join(__dirname, '../../../build'),
  gaToken: process.env.GA_TOKEN || '',
  gtmToken: process.env.GTM_TOKEN || '',
  robotsPolicy: process.env.ROBOTS_POLICY || 'all',
  smtp: getSMTPEnv(process.env),
  captchaSiteKey: process.env.CAPTCHA_SITE_KEY,
  siteEnv: process.env.SITE_ENV || '',
};

module.exports = config;
