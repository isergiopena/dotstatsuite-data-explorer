import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useIntl } from 'react-intl';
import { innerPalette } from '@sis-cc/dotstatsuite-visions';
import { getIsRtl, getLocale, getHasAccessibility, getPathname } from '../selectors/router';
import { getDataflowName } from '../selectors/sdmx';
import { outerPalette } from '../lib/settings';
import { formatMessage } from '../i18n';
import messages from './messages';

const View = ({ isRtl, lang, dataflowName, isA11y, pathname }) => {
  const intl = useIntl();

  return (
    <Helmet htmlAttributes={{ lang, dir: isRtl ? 'rtl' : 'ltr' }}>
      <title>
        {formatMessage(intl)(R.propOr(R.prop('/', messages), pathname, messages), { dataflowName })}
      </title>
      {isA11y && (
        <style type="text/css">{`
          :focus {
            outline-color: ${outerPalette.highlight1 || innerPalette.highlight1} !important;
          }
        `}</style>
      )}
    </Helmet>
  );
};

View.propTypes = {
  isRtl: PropTypes.bool,
  isA11y: PropTypes.bool,
  lang: PropTypes.string,
  dataflowName: PropTypes.string,
  pathname: PropTypes.string,
};

export default connect(
  createStructuredSelector({
    isRtl: getIsRtl,
    isA11y: getHasAccessibility,
    lang: getLocale,
    dataflowName: getDataflowName,
    pathname: getPathname,
  }),
)(View);
