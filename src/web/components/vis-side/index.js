import PropTypes from 'prop-types';
import * as R from 'ramda';
import React, { Fragment, useState } from 'react';
import { useDispatch } from 'react-redux';
import { changeDataquery, resetFilters } from '../../ducks/sdmx';
import { FormattedMessage } from '../../i18n';
import LastNPeriod from './lastn-period';
import Period from './period';
import DataAvailability from './data-availability';
import { ClearFilters, FiltersCurrent, UsedFilters } from './used-filter';
import FilterPeriod from './filter-period';
import AdvancedPopup from './advanced-popup';
import Filters from './filters';

const Side = ({ isVisPage }) => {
  const [
    { isOpen, label, items, id, spotlight, expandedIds, filtersDefaultProps },
    setAdvancedFilterState,
  ] = useState({
    isOpen: false,
    items: [],
    id: null,
    label: null,
    spotlight: null,
    expandedIds: null,
    filtersDefaultProps: {},
  });

  const dispatch = useDispatch();

  return (
    <Fragment>
      {!isVisPage && (
        <FiltersCurrent>
          <UsedFilters />
          <ClearFilters
            onDelete={dispatch(resetFilters)}
            label={<FormattedMessage id="vx.filters.current.clear" />}
          />
        </FiltersCurrent>
      )}
      <FilterPeriod>
        <Period />
        <LastNPeriod />
      </FilterPeriod>
      <AdvancedPopup
        isOpen={isOpen}
        items={items}
        id={id}
        title={label}
        onClose={() =>
          setAdvancedFilterState({
            isOpen: false,
            items: [],
            id: null,
            spotlight: null,
            expandedIds: null,
          })
        }
        changeSelection={(id, selection, state) => {
          setAdvancedFilterState({
            isOpen: false,
            items: [],
            id: null,
            spotlight: null,
            expandedIds: null,
            filtersDefaultProps: R.assoc(id, state, filtersDefaultProps),
          });
          dispatch(changeDataquery(id, selection));
        }}
        defaultExpandedIds={expandedIds}
        defaultSpotlight={spotlight}
      />
      <Filters setAdvancedFilterState={setAdvancedFilterState} defaultProps={filtersDefaultProps} />
      <DataAvailability />
    </Fragment>
  );
};

Side.propTypes = {
  isVisPage: PropTypes.bool,
};

export default Side;
