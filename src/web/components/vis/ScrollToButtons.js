import React from 'react';
import cx from 'classnames';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: '100%',
    minWidth: 0,
    backgroundColor: '#0549ab47',
    padding: theme.spacing(1, 2),
    zIndex: 10,
  },
  rotate: {
    transform: 'rotate(90deg)',
  },
  scrollLeftButton: {
    position: 'fixed',
    bottom: 65,
    right: 70,
  },
  scrollTopButton: {
    position: 'fixed',
    bottom: 65,
    right: 16,
  },
  display: {
    display: 'none',
  },
}));

const ScrollToButtons = ({ scrollsPos }) => {
  const classes = useStyles();
  const [isTopDisplayed, setIsTopDisplayed] = React.useState(scrollsPos?.scrollY == 0);
  const [isLeftDisplayed, setIsLeftDisplayed] = React.useState(scrollsPos?.scrollX == 0);

  React.useEffect(() => {
    setIsTopDisplayed(scrollsPos?.scrollY == 0);
    setIsLeftDisplayed(scrollsPos?.scrollX == 0);
  }, [scrollsPos]);

  return (
    <>
      {R.map(
        ({ className, rotate, action, isDisplayed }) => (
          <Button
            key={className}
            variant="contained"
            color="primary"
            component="span"
            className={cx(className, { [classes.display]: isDisplayed })}
            classes={{ root: classes.root, label: rotate }}
            onClick={action}
          >
            «
          </Button>
        ),
        [
          {
            className: classes.scrollTopButton,
            rotate: classes.rotate,
            action: () => window.scrollTo({ top: 0, behavior: 'smooth' }),
            isDisplayed: isTopDisplayed,
          },
          {
            className: classes.scrollLeftButton,
            action: () => window.scrollTo({ left: 0, behavior: 'smooth' }),
            isDisplayed: isLeftDisplayed,
          },
        ],
      )}
    </>
  );
};

export default ScrollToButtons;
