import * as R from 'ramda';
import { extractId } from './valueParser';

export { default as searchParser } from './searchParser';
export { default as configParser } from './configParser';
export { default as searchConstants } from './constants';

/*
 * a facet is irrelevant if:
 * - results count is equal to ALL values count if it doesn't have values with count less than the ALL values count
 */

export const rejectIrrelevantFacets = ({ count }) => {
  return R.reduce((acc, el) => {
    const valuesWithCountLessNbs = R.filter(value => {
      return value.count && value.count !== count;
    })(R.prop('values')(el));
    const parentValues = R.compose(
      R.map(extractId),
      R.reject(R.isNil),
      R.uniq,
      R.unnest,
      R.pluck('path'),
    )(valuesWithCountLessNbs);
    const parentsToAdd = R.difference(
      parentValues,
      R.pluck('code')(valuesWithCountLessNbs),
    );
    const newFacetValues = R.compose(
      R.unnest,
      R.append(
        R.map(parent => R.find(R.propEq('code', parent))(el.values))(
          parentsToAdd,
        ),
      ),
    )(valuesWithCountLessNbs);
    if (!R.isEmpty(newFacetValues))
      acc.push({
        ...el,
        values: R.sortWith(
          [R.ascend(R.prop('order')), R.ascend(R.prop('label'))],
          newFacetValues,
        ),
      });
    return acc;
  }, []);
};
