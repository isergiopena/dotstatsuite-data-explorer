import React from 'react';
import { is } from 'ramda';
import PropTypes from 'prop-types';
import sanitizeHtml from 'sanitize-html';
import { sanitizeOptions } from '../lib/settings';

const SanitizedInnerHTML = ({ html = '', ...rest }) => {
  if (!is(String, html)) {
    return html;
  }
  return (
    <span dangerouslySetInnerHTML={{ __html: sanitizeHtml(html, sanitizeOptions) }} {...rest} />
  );
};

SanitizedInnerHTML.propTypes = {
  html: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

export default SanitizedInnerHTML;
