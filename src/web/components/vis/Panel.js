import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { changeActionId } from '../../ducks/vis';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(1, 0, 1, 0),
    padding: theme.spacing(2),
  },
}));
const Panel = ({ children, actionId }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  return (
    <Paper elevation={2} className={classes.container}>
      <IconButton
        style={{ float: 'right' }}
        size="small"
        color="primary"
        onClick={() => dispatch(changeActionId(actionId))}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
      {children}
    </Paper>
  );
};
Panel.propTypes = {
  children: PropTypes.node,
  actionId: PropTypes.string,
};
export default Panel;
