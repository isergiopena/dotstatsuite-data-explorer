import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import qs from 'qs';
import * as R from 'ramda';
import { useTheme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import Divider from '@material-ui/core/Divider';
import { useIntl } from 'react-intl';
import { formatMessage } from '../../../i18n';
import { Button, Menu } from './helpers';
import messages, { toolbarMessages, visDlMessages } from '../../messages';
import { getViewer } from '../../../selectors/router';
import { getIsUserLogged } from '../../../selectors/app';
import {
  getDataFileRequestArgs,
  getExternalResources,
  getIsCsvFileLinkHandled,
  getIsDataUrlTooLong,
} from '../../../selectors/sdmx';
import {
  CHART_IDS,
  EXCEL,
  MICRODATA,
  PNG,
  TABLE,
} from '../../../utils/constants';
import { requestVisDataFile } from '../../../ducks/sdmx';
import { downloadPng, downloadExcel } from '../../../ducks/vis';
import html2canvas from 'html2canvas';
import { ID_VIEWER_COMPONENT } from '../../../css-api';
import { cellsLimit } from '../../../lib/settings';
import { getMicrodataFileRequestArgs } from '../../../selectors/microdata';
import Link from './Link';
import ActionButton from './ActionButton';
import useOverview from '../../../hooks/useOverview';

const useStyles = makeStyles(theme => ({
  linkEnd: {
    margin: 0,
    padding: theme.spacing(0.75, 2),
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'end',
  },
  linkStart: {
    margin: 0,
    padding: theme.spacing(0.75, 2),
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'start',
  },
  linkIcon: {
    height: 20,
    paddingRight: theme.spacing(1),
    alignItems: 'center',
  },
}));

const getCsvUrl = ({ url, params }) =>
  `${url}${qs.stringify(params, { addQueryPrefix: true })}`;

const Component = ({ loading, viewerProps }) => {
  const classes = useStyles();
  const intl = useIntl();
  const dispatch = useDispatch();
  const theme = useTheme();
  const viewerId = useSelector(getViewer);
  const isUserLogged = useSelector(getIsUserLogged);
  const isDataUrlTooLong = useSelector(getIsDataUrlTooLong);
  const filteredDataCsvArgs = useSelector(getDataFileRequestArgs(false));
  const unfilteredDataCsvArgs = useSelector(getDataFileRequestArgs(true));
  const filteredMicrodataCsvArgs = useSelector(
    getMicrodataFileRequestArgs(false),
  );
  const unfilteredMicrodataCsvArgs = useSelector(
    getMicrodataFileRequestArgs(true),
  );
  const isCsvFileLinkHandled = useSelector(getIsCsvFileLinkHandled);
  const externalResources = useSelector(getExternalResources);
  const filteredArgs =
    viewerId === MICRODATA ? filteredMicrodataCsvArgs : filteredDataCsvArgs;
  const unfilteredArgs =
    viewerId === MICRODATA ? unfilteredMicrodataCsvArgs : unfilteredDataCsvArgs;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const overviewProps = useOverview();
  const enhanceTableProps = R.pipe(
    R.set(
      R.lensPath(['footerProps', 'copyright', 'content']),
      formatMessage(intl)(messages.viewerLinkLabel),
    ),
    R.set(
      R.lensPath(['footerProps', 'copyright', 'link']),
      formatMessage(intl)(messages.viewerLink),
    ),
    R.over(R.lensPath(['headerProps', 'disclaimer']), disclaimer =>
      R.isNil(disclaimer)
        ? null
        : formatMessage(intl)(messages.incompleteExcelData, { cellsLimit }),
    ),
  );

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<GetAppIcon />}
        selected={Boolean(anchorEl)}
        loading={loading}
        onClick={openMenu}
        aria-haspopup="true"
        data-testid="downloads-button"
        aria-expanded={Boolean(anchorEl)}
        isToolTip
      >
        {formatMessage(intl)(toolbarMessages.download)}
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {viewerId === TABLE && (
          <ActionButton
            intl={intl}
            id="excel.selection"
            key="excel.selection"
            onClick={() => {
              dispatch(
                downloadExcel({
                  ...enhanceTableProps(viewerProps),
                  overviewProps,
                  id: EXCEL,
                  theme,
                }),
              );
              closeMenu();
            }}
          />
        )}
        {viewerId === MICRODATA && (
          <ActionButton intl={intl} id="microdata.download.excel.selection" />
        )}
        {R.includes(viewerId, R.values(CHART_IDS)) && (
          <ActionButton
            id="chart.selection"
            intl={intl}
            onClick={() => {
              const { width, height } = viewerProps?.chartOptions?.base;
              let options = { scale: 2, scrollY: -window.scrollY };
              if (!R.isNil(width)) options.width = width;
              if (!R.isNil(height)) options.height = height;
              dispatch(
                downloadPng(
                  () =>
                    html2canvas(
                      document.getElementById(ID_VIEWER_COMPONENT),
                      options,
                    ),
                  PNG,
                ),
              );
              closeMenu();
            }}
          />
        )}
        {isUserLogged || isDataUrlTooLong || !isCsvFileLinkHandled
          ? [
              <ActionButton
                intl={intl}
                key="csv.selection"
                id={
                  viewerId === MICRODATA
                    ? 'microdata.download.csv.selection'
                    : 'data.download.csv.selection'
                }
                onClick={() => {
                  dispatch(requestVisDataFile({ isDownloadAllData: false }));
                  closeMenu();
                }}
              />,
              <ActionButton
                intl={intl}
                key="csv.all"
                id={
                  viewerId === MICRODATA
                    ? 'microdata.download.csv.all'
                    : 'data.download.csv.all'
                }
                disabled={viewerId === MICRODATA}
                onClick={() => {
                  dispatch(requestVisDataFile({ isDownloadAllData: true }));
                  closeMenu();
                }}
              />,
            ]
          : [
              <li
                key="csv.selection"
                role="menuitem"
                tabIndex={0}
                aria-label={formatMessage(intl)(
                  R.prop(
                    `${
                      viewerId === MICRODATA ? 'microdata' : 'data'
                    }.download.csv.selection`,
                    visDlMessages,
                  ),
                )}
              >
                <Link
                  callback={() => {
                    dispatch(requestVisDataFile({ isDownloadAllData: false }));
                    closeMenu();
                  }}
                  classes={classes}
                  filename={`${filteredArgs.filename}.csv`}
                  id="csv.selection"
                  justifyContent="start"
                  label={formatMessage(intl)(
                    R.prop(
                      `${
                        viewerId === MICRODATA ? 'microdata' : 'data'
                      }.download.csv.selection`,
                      visDlMessages,
                    ),
                  )}
                  link={getCsvUrl(filteredArgs)}
                />
              </li>,
              <li
                key="csv.all"
                role="menuitem"
                tabIndex={0}
                aria-label={formatMessage(intl)(
                  R.prop(
                    `${
                      viewerId === MICRODATA ? 'microdata' : 'data'
                    }.download.csv.all`,
                    visDlMessages,
                  ),
                )}
              >
                <Link
                  callback={() => {
                    dispatch(requestVisDataFile({ isDownloadAllData: true }));
                    closeMenu();
                  }}
                  classes={classes}
                  disabled={viewerId === MICRODATA}
                  filename={`${unfilteredArgs.filename}.csv`}
                  id="csv.all"
                  justifyContent="start"
                  label={formatMessage(intl)(
                    R.prop(
                      `${
                        viewerId === MICRODATA ? 'microdata' : 'data'
                      }.download.csv.all`,
                      visDlMessages,
                    ),
                  )}
                  link={getCsvUrl(unfilteredArgs)}
                />
              </li>,
            ]}
        {R.not(R.isEmpty(externalResources)) && <Divider />}
        {R.map(
          ({ key, ...props }) => (
            <Link
              {...props}
              key={key}
              callback={() => closeMenu()}
              classes={classes}
              justifyContent="end"
            />
          ),
          externalResources,
        )}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  loading: PropTypes.bool,
  viewerProps: PropTypes.object,
  overviewProps: PropTypes.object,
};

export default Component;
