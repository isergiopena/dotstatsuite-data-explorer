import React from 'react';
import Typography from '@material-ui/core/Typography';
import { getRange } from '../../../selectors/microdata';
import { useSelector } from 'react-redux';
import { FormattedMessage } from '../../../i18n';

const Title = () => {
  const { count } = useSelector(getRange);

  return (
    <Typography variant="body2" tabIndex={0}>
      <FormattedMessage id="microdata.count" values={{ count }} />
    </Typography>
  );
};

export default Title;
