const axios = require('axios');
const R = require('ramda');

axios
  .post('http://localhost:8282/api/auth/register', {
    firstname: 'Toto',
    lastname: 'Toto',
    email: 'eric.basley@redpelicans.com',
    password: '123456789',
  })
  .then(R.prop('data'))
  .then(console.log);
