import { changeVibe, testidSelector } from './utils';

describe('charts : choro map', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('open chart menu', async () => {
    const chartButton = await page.$(testidSelector('chart-button'));
    await chartButton.click();
    await page.waitForSelector(testidSelector('chart-menu'));
  });
  it('select choro map option from chart menu', async () => {
    const chartMenu = await page.$(testidSelector('chart-menu'));
    const choroMapOption = await chartMenu.$(
      testidSelector('map-world:countries-button'),
    );
    await choroMapOption.click();
    await page.waitForSelector('.rcw-chart__chart__areas');
  });
  it('check Australia got colorizeded in the map', async () => {
    const areas = await page.$('.rcw-chart__chart__areas');
    const australianArea = await areas.$(testidSelector('AU'));
    const fillColor = await australianArea.$eval('path', p => p.style.fill);
    expect(fillColor).not.toEqual('rgb(255, 255, 255)');
  });
  it('reload the page and check that charts still got rendered', async () => {
    await page.reload();
    await page.waitForSelector('.rcw-chart__chart__areas');
  });
});
