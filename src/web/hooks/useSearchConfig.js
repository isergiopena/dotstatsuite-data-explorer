import { useQuery } from 'react-query';
import searchApi from '../api/search';
import { homeFacetIds, valueIcons } from '../lib/settings/index';

export default ({ localeId }) => {
  const ctx = {
    method: 'getConfig',
    requestArgs: { lang: localeId, rows: 0, fl: homeFacetIds },
    parserArgs: { facetIds: homeFacetIds, config: { valueIcons } },
  };

  const queryKey = `@@search/${ctx.method} --> ${JSON.stringify(ctx.requestArgs)}`;
  return useQuery(queryKey, () => searchApi(ctx));
};
