import React, { useMemo } from 'react';
import * as R from 'ramda';
import { useIntl } from 'react-intl';
import { useSelector } from 'react-redux';
import { Loading, NoData } from '@sis-cc/dotstatsuite-visions';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import FiltersHelp from './filters-help';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage, formatMessage } from '../i18n';
import { getIsFull } from '../selectors';
import {
  getAppBarsOffset,
  getExtAuthOptions,
  getIsPending,
} from '../selectors/app';
import { getIsMicrodata } from '../selectors/microdata';
import { getDataflow } from '../selectors/router';
import Side from './vis-side';
import { ParsedDataVisualisation } from './vis/vis-data';
import MicrodataTitle from './vis/microdata/title';
import NarrowFilters from './vis-side/side-container';
import Page from './Page';
import { ID_VIS_PAGE } from '../css-api';
import useEventListener from '../utils/useEventListener';
import ScrollToButtons from './vis/ScrollToButtons';
import {
  MARGE_RATIO,
  MARGE_SIZE,
  SIDE_WIDTH,
  SMALL_SIDE_WIDTH,
} from '../utils/constants';
import useSdmxStructure from '../hooks/useSdmxStructure';
import useSdmxData from '../hooks/useSdmxData';
import useSdmxAvailableConstraints from '../hooks/useSdmxAvailableConstraints';
import {
  getDatasource,
  getRefinedDataRange,
  getUsedFilters,
} from '../selectors/sdmx';
import SpaceAuthDialog from './SpaceAuthDialog';
import UsedFilters from './UsedFilters';
import useFilterFrequency from '../hooks/useFilterFrequency';
import { countNumberOf } from '../utils';
import PopupSurvey from './PopupSurvey';
import usePopupSurvey from '../hooks/usePopupSurvey';
import useUsedFilters from '../hooks/useUsedFilters';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  visPage: {
    overflow: 'visible',
    backgroundColor: theme.palette.background.default,
    marginTop: ({ isFull, appBarsOffset }) => (isFull ? 0 : appBarsOffset),
    padding: `0 ${MARGE_SIZE}%`,
    zIndex: 1,
    [theme.breakpoints.down('xs')]: {
      padding: '0 16px 0',
    },
  },
  gutters: {
    display: 'flex',
    margin: 0,
    flexWrap: 'nowrap',
    backgroundColor: theme.palette.background.default,
    marginRight: '-15px',
    [theme.breakpoints.down('xs')]: {
      marginRight: 0,
    },
    [theme.breakpoints.down('sm')]: {
      flexWrap: 'wrap',
    },
  },
  visContainer: {
    marginTop: theme.spacing(-2.5),
    paddingTop: theme.spacing(2.5),
    backgroundColor: theme.palette.background.default,
  },
  space: {
    position: 'sticky',
    left: 0,
    backgroundColor: 'red',
    maxWidth: ({ width }) => width * MARGE_RATIO,
  },
  usedFilterslabel: {
    paddingRight: 5,
    color: theme.palette.grey[700],
    ...R.pathOr({}, ['mixins', 'expansionPanel', 'title'], theme),
  },
  side: {
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      position: 'sticky',
      '&::after': {
        content: '""',
        position: 'absolute',
        backgroundColor: theme.palette.background.default,
        top: '0',
        left: `-${MARGE_SIZE + 3}%`,
        width: `${MARGE_SIZE + 3}%`,
        height: '100%',
      },
      '&::before': {
        content: '""',
        position: 'absolute',
        backgroundColor: theme.palette.background.default,
        top: '0',
        right: `-${MARGE_SIZE + 3}%`,
        width: `${MARGE_SIZE + 3}%`,
        height: '100%',
      },
    },
    [theme.breakpoints.only('md')]: {
      marginRight: theme.spacing(2),
      minWidth: SMALL_SIDE_WIDTH,
      maxWidth: SMALL_SIDE_WIDTH,
    },
    [theme.breakpoints.up('lg')]: {
      marginRight: theme.spacing(2),
      minWidth: SIDE_WIDTH,
      maxWidth: SIDE_WIDTH,
    },
    marginBottom: theme.spacing(2.5),
  },
  dataPoints: {
    marginTop: ({ isNarrow }) => (isNarrow ? 0 : theme.spacing(-2.5)),
  },
}));

export default () => {
  const theme = useTheme();
  const intl = useIntl();
  const range = useSelector(getRefinedDataRange);
  const isNarrow = useMediaQuery(theme.breakpoints.down('sm'));
  const isFull = useSelector(getIsFull());
  const isMicrodata = useSelector(getIsMicrodata);
  const visPageRef = React.useRef(null);
  const [scrollsPos, setScrollsPos] = React.useState({
    scrollY: 0,
    scrollX: 0,
  });
  const [visPageWidth, setVisPageWidth] = React.useState(0);
  const appBarsOffset = useSelector(getAppBarsOffset);
  const classes = useStyles({
    width: visPageWidth,
    isFull,
    isNarrow,
    appBarsOffset,
  });
  const isParsingData = useSelector(getIsPending('parsingData'));
  const datasource = useSelector(getDatasource);
  const dataflow = useSelector(getDataflow);
  useEventListener('resize', () => {
    if (visPageRef.current) {
      setVisPageWidth(visPageRef.current.offsetWidth);
    }
  });
  useEventListener('scroll', () =>
    setScrollsPos({
      scrollY: window.scrollY,
      scrollX: window.scrollX,
    }),
  );

  React.useLayoutEffect(() => {
    if (visPageRef.current) {
      setVisPageWidth(visPageRef.current.offsetWidth);
    }
  }, []);

  const { isLoading: isLoadingStructure, isError, error } = useSdmxStructure();
  const { isLoading: isFetchingData } = useSdmxData();
  const isLoadingData = isFetchingData || isParsingData;
  useSdmxAvailableConstraints();
  let errorMessage = null;
  if (isError) {
    const status = error?.response?.status;
    // the space or datasource can be invalid (missing or mismatch with config/settings)
    // the sdmx structure is in error in this case,
    // we just check before if it's related to this case before jumping to the next case
    if (R.isEmpty(datasource))
      errorMessage = (
        <FormattedMessage
          id="log.error.sdmx.invalid.space"
          values={{ space: dataflow.datasourceId }}
        />
      );
    else if (status === 404)
      errorMessage = <FormattedMessage id="log.error.sdmx.404" />;
    else if (R.includes(status, [401, 402, 403]))
      errorMessage = <FormattedMessage id="log.error.sdmx.40x" />;
    else errorMessage = <FormattedMessage id="log.error.sdmx.xxx" />;
  }
  const { isShowing, surveyLink, surveyImage, hasSurvey } = usePopupSurvey();

  const { hasFailed, credentials, isAnonymous } = useSelector(
    getExtAuthOptions(datasource.id),
  );
  const isExtAuthenticated = !!credentials || isAnonymous;
  const isExtAuthCandidate = datasource.hasExternalAuth && !isExtAuthenticated;
  if (isExtAuthCandidate) return <SpaceAuthDialog datasource={datasource} />;
  const { frequencyFilter = [], periodFilter = [] } = useFilterFrequency();
  const selection = useSelector(getUsedFilters);
  const counter = countNumberOf(
    R.concat(selection, R.concat(frequencyFilter, periodFilter)),
  );

  const {
    items,
    onDelete,
    labelRenderer,
    labels,
    onDeleteAll,
    clearAllLabel,
  } = useUsedFilters();
  const { specialFiltersItems, onDeleteSpecialFilters } = useFilterFrequency();
  const data = useMemo(
    () => [
      { items, labelRenderer, onDelete },
      {
        items: specialFiltersItems,
        labelRenderer: R.prop('label'),
        onDelete: onDeleteSpecialFilters,
      },
    ],
    [items, specialFiltersItems],
  );
  const total = R.propOr(0, 'total', range);
  return (
    <React.Fragment>
      {hasFailed && (
        <SpaceAuthDialog datasource={datasource} hasFailed={hasFailed} />
      )}
      {isLoadingStructure && (
        <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
          <FormattedMessage id="de.visualisation.loading" />
        </Typography>
      )}
      <Page id={ID_VIS_PAGE} classNames={[classes.visPage]} ref={visPageRef}>
        {isError && (
          <div>
            <div className={classes.gutters}>
              <NoData message={errorMessage} />
            </div>
          </div>
        )}

        {isLoadingStructure && (
          <div>
            <div className={classes.gutters}>
              <Loading
                message={<FormattedMessage id="de.visualisation.loading" />}
              />
            </div>
          </div>
        )}

        {!isError && !isLoadingStructure && (
          <Grid container>
            {hasSurvey && (
              <PopupSurvey
                url={surveyLink}
                img={surveyImage}
                isShowing={isShowing}
              />
            )}
            {R.not(isFull) && R.not(isMicrodata) && (
              <div className={classes.gutters}>
                {!isNarrow && <FiltersHelp />}
                <div className={classes.space}></div>
              </div>
            )}
            <div
              className={classes.gutters}
              wrap={isNarrow ? 'wrap' : 'nowrap'}
            >
              {R.not(isFull) && R.not(isMicrodata) && (
                <div className={classes.side}>
                  <NarrowFilters isNarrow={isNarrow}>
                    <Side isVisPage />
                  </NarrowFilters>
                </div>
              )}
              <Grid>
                {R.not(isFull) && R.not(isMicrodata) && counter !== 0 && (
                  <UsedFilters
                    counter={counter}
                    maxWidth={visPageWidth}
                    data={data}
                    labels={labels}
                    clearAllLabel={clearAllLabel}
                    onDeleteAll={onDeleteAll}
                    dataCount={R.equals(0, total) ? null : total}
                    dataCountLabel={formatMessage(intl)(messages.dataPoints, {
                      obsCount: R.propOr(0, 'total', range),
                    })}
                  />
                )}
                {R.not(isMicrodata) && counter === 0 && !R.equals(0, total) && (
                  <div className={classes.dataPoints}>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      tabIndex={0}
                    >
                      <FormattedMessage
                        id="de.vis.data.points"
                        values={{ obsCount: R.propOr(0, 'total', range) }}
                      />
                    </Typography>
                  </div>
                )}
                <Grid item xs={12} className={classes.visContainer}>
                  {isMicrodata && !isLoadingData && <MicrodataTitle />}
                  <ParsedDataVisualisation visPageWidth={visPageWidth} />
                </Grid>
              </Grid>
            </div>
            <ScrollToButtons scrollsPos={scrollsPos} />
          </Grid>
        )}
      </Page>
    </React.Fragment>
  );
};
