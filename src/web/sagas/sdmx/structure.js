import sdmxApi from '../../api/sdmx';
import * as R from 'ramda';
import dateFns from 'date-fns';
import { select, put, call, takeLatest, putResolve } from 'redux-saga/effects';
import {
  getActualContentConstraintsDefaultSelection,
  getDataflowExternalReference,
  parseStructure as sdmxjsParseStructure,
  getDataquery as sdmxjsGetDataquery,
  getRequestArgs as sdmxjsGetRequestArgs,
  getFrequencyArtefact,
  getExternalResources,
  parseExternalReference,
  getArtefactCategories as sdmxjsParseArtefactCategories,
  getDataflowDescription,
  getActualContentConstraintsArtefact,
  getNotDisplayedCombinations,
} from '@sis-cc/dotstatsuite-sdmxjs';
import {
  getConstraints,
  getLocale,
  getPeriod,
  getDataquery,
  getTableLayout,
  getHasDataAvailability,
  getHighlightedConstraints,
  getLastNObservations,
  getDataflow,
  getLastNMode,
  getHasLastNObservations,
} from '../../selectors/router';
import { getHierarchies } from '../../selectors/sdmx';
import {
  HANDLE_STRUCTURE,
  PARSE_STRUCTURE,
  EXTERNAL_REFERENCE,
  HANDLE_EXTERNAL_RESOURCES,
  REQUEST_EXTERNAL_RESOURCES,
  REQUEST_ALL_HCL,
} from '../../ducks/sdmx';
import {
  setPending,
  pushLog,
  flushLog,
  LOG_ERROR_SDMX_STRUCTURE,
  UPDATE_SAGA_KEY,
} from '../../ducks/app';
import { getDefaultSelection } from '../../lib/search/sdmx-constraints-bridge';
import {
  getDefaultRouterParams,
  getDefaultSelectionContentConstrained,
  getOnlyHasDataDimensions,
} from '../../lib/sdmx';
import {
  getAutomatedSelections,
  selectAutomatedSelections,
  parseDataquery,
} from '../../lib/sdmx/selection';
import {
  defaultLastNPeriods,
  getSpaceFromDatasourceId,
  getSpaceFromUrl,
  isDefaultTimeDimensionInverted,
  sdmxPeriodBoundaries,
} from '../../lib/settings';
import { renameKeys } from '../../utils';

import { getHierarchicalCodelistsReferences } from '../../lib/sdmx/hierarchical-codelist';
import { LASTNOBSERVATIONS, LASTNPERIODS } from '../../utils/used-filter';
import {
  getDataStructureAnnotations,
  getDataflowAnnotations,
} from '../../lib/sdmx/accessors/structure';
import { getSagaKey } from '../../selectors/app';

const isDev = process.env.NODE_ENV === 'development';

function* requestExternalResources({ dataflow }) {
  const dataflowId = R.prop('dataflowId')(dataflow);
  const pendingId = `getExternalResources/${dataflowId}`;
  const locale = yield select(getLocale);
  const externalUrl = R.prop('externalUrl', dataflow);
  const externalReference = parseExternalReference(
    {
      isExternalReference: !R.isNil(externalUrl),
      links: [{ rel: 'external', href: externalUrl }],
    },
    'dataflow',
  );

  const space = R.isNil(externalReference)
    ? getSpaceFromDatasourceId(R.prop('datasourceId', dataflow))
    : getSpaceFromUrl(R.path(['datasource', 'url'], externalReference));

  const datasource = space || R.prop('datasource', externalReference);
  const requestArgs = sdmxjsGetRequestArgs({
    identifiers: R.defaultTo(
      R.pipe(
        R.pickAll(['agencyId', 'dataflowId', 'version']),
        renameKeys({ dataflowId: 'code' }),
      )(dataflow),
      R.prop('identifiers', externalReference),
    ),
    datasource,
    type: 'dataflow',
    withPartialReferences: true,
    locale,
  });

  try {
    yield put(setPending(pendingId, true));
    yield put(flushLog(LOG_ERROR_SDMX_STRUCTURE));
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const structure = yield call(sdmxApi, {
      method: 'getStructureExternalResources',
      requestArgs,
    });
    yield put(setPending(pendingId, false));
    yield put({
      type: HANDLE_EXTERNAL_RESOURCES,
      externalResources: { [dataflowId]: getExternalResources(structure) },
    });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      const log = {
        status: error.response.status,
        statusText: error.response.statusText,
        url: requestArgs.url,
        headers: requestArgs.headers,
      };
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
    }
  }
}

const getDataIncreaseAnnotation = structure =>
  R.pipe(
    getDataflowAnnotations,
    R.find(R.propEq('type', 'MAX_TABLE_DATA')),
  )(structure);

const hasTimePeriodDesc = accessor => structure =>
  R.pipe(
    accessor,
    R.find(R.propEq('type', 'LAYOUT_TIME_PERIOD_DESC')),
    R.complement(R.isNil),
  )(structure);

const getHiddenValuesAnnotations = accessor => structure =>
  R.pipe(accessor, R.find(R.propEq('type', 'NOT_DISPLAYED')))(structure);

const haslayoutCellAlign = accessor => structure =>
  R.pipe(accessor, R.find(R.propEq('type', 'LAYOUT_CELL_ALIGN')))(structure);

function* parseStructure({ structure, queryKey }) {
  const sagaId = 'parseStructure';
  const key = yield select(getSagaKey(sagaId));
  if (key === queryKey && !R.isNil(queryKey)) return;

  yield put({ type: UPDATE_SAGA_KEY, sagaKey: queryKey, sagaId });

  const currentLayout = yield select(getTableLayout);
  const currentDataquery = yield select(getDataquery);
  const currentLastNMode = yield select(getLastNMode);
  const currentPeriod = yield select(getPeriod);
  const currentLastNObservations = yield select(getLastNObservations);
  const hasLastNObservations = yield select(getHasLastNObservations);
  const { dataflowId, version, agencyId } = yield select(getDataflow);
  const locale = yield select(getLocale);
  const isTimePeriodInversed = R.either(
    hasTimePeriodDesc(getDataflowAnnotations),
    hasTimePeriodDesc(getDataStructureAnnotations),
  )(structure);
  const hasHiddenValuesAnnotation = R.either(
    getHiddenValuesAnnotations(getDataflowAnnotations),
    getHiddenValuesAnnotations(getDataStructureAnnotations),
  )(structure);

  const hiddenValuesAnnotation = hasHiddenValuesAnnotation
    ? getNotDisplayedCombinations(hasHiddenValuesAnnotation)
    : {};

  const layoutAlignAnnot = R.either(
    haslayoutCellAlign(getDataflowAnnotations),
    haslayoutCellAlign(getDataStructureAnnotations),
  )(structure);

  const textAlign = R.either(
    R.path(['texts', locale]),
    R.prop('title'),
  )(layoutAlignAnnot);

  try {
    const externalReference = getDataflowExternalReference(structure);
    const externalRefSpace = getSpaceFromUrl(
      R.path(['datasource', 'url'], externalReference),
    );
    const enhancedExternalReference = R.when(
      R.always(externalRefSpace),
      R.assoc('datasource', externalRefSpace),
    )(externalReference);
    if (!R.isNil(externalReference)) {
      yield put({
        type: EXTERNAL_REFERENCE,
        externalReference: enhancedExternalReference,
      });
      return;
    }

    const {
      dimensions,
      attributes,
      selection,
      timePeriod,
      externalResources,
      contentConstraints,
      layout,
      params,
      name,
      microdataDimensionId,
    } = sdmxjsParseStructure(structure);

    const automatedSelections = getAutomatedSelections(structure);
    const constraintsArtefact = getActualContentConstraintsArtefact(structure);
    const constraints = yield select(getConstraints);
    const highlightedContraints = yield select(getHighlightedConstraints);
    // content constraints should not be apply if empty or undefined on the default selection
    const hasDA = yield select(getHasDataAvailability) &&
      !R.isEmpty(contentConstraints || {});

    const getObservationCount = R.pipe(
      R.propOr([], 'annotations'),
      R.find(R.propEq('id', 'obs_count')),
      R.prop('title'),
    );

    const observationsType = R.path(
      [
        'data',
        'dataStructures',
        0,
        'dataStructureComponents',
        'measureList',
        'primaryMeasure',
        'localRepresentation',
        'textFormat',
        'textType',
      ],
      structure,
    );

    let hierarchies = null;
    const hCodelistRefs = getHierarchicalCodelistsReferences(structure);
    if (!R.isEmpty(hCodelistRefs)) {
      yield putResolve({
        type: REQUEST_ALL_HCL,
        references: hCodelistRefs,
      });
      hierarchies = yield select(getHierarchies);
    }

    const structureSelection = hasDA
      ? getActualContentConstraintsDefaultSelection({
          selection,
          contentConstraints,
        })
      : selection;

    const defaultSelection = hasDA
      ? R.pipe(
          getOnlyHasDataDimensions,
          R.filter(dim =>
            R.pipe(R.propOr([], 'values'), values =>
              hiddenValuesAnnotation[dim.id]
                ? !R.isEmpty(
                    R.difference(
                      hiddenValuesAnnotation[dim.id],
                      R.pluck('id')(values),
                    ),
                  )
                : true,
            )(dim),
          ),
          dimensions =>
            getDefaultSelection(
              dimensions,
              structureSelection,
              constraints,
              highlightedContraints,
            ),
          getDefaultSelectionContentConstrained(contentConstraints),
          R.reject(R.isEmpty),
        )(dimensions)
      : getDefaultSelection(dimensions, structureSelection, constraints);

    const dataquery = R.isEmpty(currentDataquery)
      ? sdmxjsGetDataquery(
          dimensions,
          selectAutomatedSelections(
            defaultSelection,
            dimensions,
            automatedSelections,
            hierarchies,
          ),
        )
      : R.pipe(
          dq => parseDataquery(dq, dimensions),
          sel =>
            selectAutomatedSelections(
              sel,
              dimensions,
              automatedSelections,
              hierarchies,
            ),
          sel => sdmxjsGetDataquery(dimensions, sel),
        )(currentDataquery);

    const isTimePeriodDisable = R.or(
      R.not(R.prop('display', timePeriod)),
      R.isNil(timePeriod),
    );

    const timePeriodContraints = isTimePeriodDisable
      ? null
      : R.prop(R.prop('id', timePeriod), contentConstraints);

    const timePeriodBoundaries = R.pipe(
      R.propOr(sdmxPeriodBoundaries, 'boundaries'),
      R.map(
        R.when(
          R.identity,
          R.pipe(date => dateFns.parse(date)),
        ),
      ),
    )(timePeriodContraints);

    const { period } = isTimePeriodDisable
      ? { period: null }
      : getDefaultRouterParams({
          hasDataAvailability: hasDA,
          frequencyArtefact: getFrequencyArtefact({ dimensions, attributes }),
          params,
          dataquery,
          timePeriodBoundaries,
          inclusiveBoundaries: R.propOr(
            [true, true],
            'includingBoundaries',
            timePeriodContraints,
          ),
        });

    let nextPeriod;
    let lastNMode;
    let lastNValue;
    const hasCurrentTimeSelection = !(
      R.isNil(currentPeriod) &&
      R.isNil(currentLastNMode) &&
      R.isNil(currentLastNObservations)
    );
    if (hasCurrentTimeSelection) {
      nextPeriod = currentPeriod;
      lastNMode =
        !R.isNil(currentPeriod) && currentLastNMode === LASTNPERIODS
          ? null
          : currentLastNMode;
      lastNValue = R.isNil(lastNMode) ? null : currentLastNObservations;
    } else {
      nextPeriod = period;
      const defaultStructLastNValue =
        R.prop('lastNPeriods', params) ||
        R.prop('lastNObservations', params) ||
        null;

      const defaultStructLastNMode = R.has('lastNPeriods', params)
        ? LASTNPERIODS
        : R.has('lastNObservations', params)
        ? LASTNOBSERVATIONS
        : null;

      const rawlastNMode =
        !R.isNil(period) && defaultStructLastNMode !== LASTNOBSERVATIONS
          ? null
          : R.defaultTo(LASTNPERIODS, defaultStructLastNMode);

      lastNMode =
        rawlastNMode === LASTNOBSERVATIONS && !hasLastNObservations
          ? null
          : rawlastNMode;
      lastNValue = R.isNil(lastNMode)
        ? null
        : R.defaultTo(defaultLastNPeriods, defaultStructLastNValue);
    }

    yield put(flushLog(LOG_ERROR_SDMX_STRUCTURE));

    const dataIncreaseAnnotation = getDataIncreaseAnnotation(structure);
    const dataRequestSize = R.isNil(dataIncreaseAnnotation)
      ? null
      : R.pipe(R.prop('title'), Number, v => (isNaN(v) ? null : v))(
          dataIncreaseAnnotation,
        );
    const timePeriodId = R.propOr('TIME', 'id', timePeriod);
    yield put({
      type: HANDLE_STRUCTURE,
      structure: {
        automatedSelections,
        annotations: R.pathOr(
          [],
          ['data', 'dataflows', 0, 'annotations'],
          structure,
        ),
        dataRequestSize,
        hiddenValuesAnnotation,
        dimensions,
        attributes,
        externalResources,
        microdataDimensionId,
        title: name,
        validFrom: R.prop('validFrom', constraintsArtefact),
        description: getDataflowDescription(structure),
        actualContentConstraints: contentConstraints,
        observationsCount: getObservationCount(constraintsArtefact),
        timePeriodArtefact: isTimePeriodDisable
          ? null
          : { ...timePeriod, timePeriodBoundaries },
        hierarchySchemes: sdmxjsParseArtefactCategories({
          artefactId: `${agencyId}:${dataflowId}(${version})`,
          data: structure.data,
        }),
        defaultPeriod: R.defaultTo(period, currentPeriod),
        textAlign,
        observationsType,
      },
      replaceStraigthHistory: {
        pathname: '/vis',
        payload: {
          filter: null,
          period: isTimePeriodDisable ? null : nextPeriod,
          lastNObservations: isTimePeriodDisable ? null : lastNValue,
          lastNMode: isTimePeriodDisable ? null : lastNMode,
          dataquery,
          layout: R.defaultTo(layout, currentLayout),
          time: !isTimePeriodInversed
            ? { [timePeriodId]: isDefaultTimeDimensionInverted }
            : { [timePeriodId]: isTimePeriodInversed },
        },
      },
    });
  } catch (error) {
    yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
  }
}

export function* watchRequestStructure() {
  yield takeLatest(REQUEST_EXTERNAL_RESOURCES, requestExternalResources);
}

export function* watchParseStructure() {
  yield takeLatest(PARSE_STRUCTURE, parseStructure);
}
