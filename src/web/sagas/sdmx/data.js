import sdmxApi from '../../api/sdmx';
import FileSaver from 'file-saver';
import * as R from 'ramda';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import { parseDataRange } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules } from '@sis-cc/dotstatsuite-components';
import {
  getDataFileRequestArgs,
  getFrequency,
  getIsCsvFileLinkHandled,
  getHiddenValuesAnnotations,
  getIsDataUrlTooLong,
} from '../../selectors/sdmx';
import {
  HANDLE_DATA,
  REQUEST_VIS_DATAFILE,
  PARSE_DATA,
} from '../../ducks/sdmx';
import {
  setPending,
  pushLog,
  LOG_ERROR_VIS_CSV_DL,
  LOG_ERROR_SDMX_DATA,
  flushLog,
  UPDATE_SAGA_KEY,
} from '../../ducks/app';
import { locales } from '../../lib/settings';
import { CHANGE_LAYOUT, csvDlStartTick } from '../../ducks/vis';
import { PARSE_MICRODATA, HANDLE_MICRODATA } from '../../ducks/microdata';
import {
  getIsMicrodata,
  getMicrodataFileRequestArgs,
} from '../../selectors/microdata';
import { getDataflow, getLocale } from '../../selectors/router';
import { getSagaKey, getIsUserLogged } from '../../selectors/app';
import { MICRODATA } from '../../utils/constants';
import { withAuthorizationHeaderGenerator } from './utils';

const isDev = process.env.NODE_ENV === 'development';

export const downloadFile = ({ response, filename }) => {
  const BOM = '\uFEFF';
  const blob = new Blob([R.concat(BOM, R.prop('data')(response))], {
    type: 'text/csv;charset=utf-8',
  });
  FileSaver.saveAs(blob, `${filename}.csv`);
};

function* requestDataFile({ pendingId, filename, requestArgs }) {
  try {
    yield put(setPending(pendingId, true));
    yield put(flushLog(LOG_ERROR_VIS_CSV_DL));
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const response = yield call(sdmxApi, {
      method: 'getDataFile',
      requestArgs,
    });
    yield call(downloadFile, { response, filename });
    yield put(setPending(pendingId, false));
    return { isSuccess: true };
  } catch (error) {
    const log = error.response
      ? {
          errorCode: error.response.data.errorCode,
          statusCode: error.response.status,
        }
      : { error };

    yield put(setPending(pendingId, false));
    yield put(pushLog({ type: LOG_ERROR_VIS_CSV_DL, log }));

    return log;
  }
}

function* requestVisDataFile(action) {
  const { isDownloadAllData } = action.payload;
  const isUserLogged = yield select(getIsUserLogged);
  const isDataUrlTooLong = yield select(getIsDataUrlTooLong);
  const isCsvLinkHandled = yield select(getIsCsvFileLinkHandled);
  if (!isUserLogged && !isDataUrlTooLong && isCsvLinkHandled) {
    yield put(csvDlStartTick());
    return;
  }

  const isMicrodata = yield select(getIsMicrodata);

  let getRequestFileArgs = getDataFileRequestArgs;
  if (isMicrodata) {
    getRequestFileArgs = getMicrodataFileRequestArgs;
  }

  const { filename, ...rawRequestArgs } = yield select(
    getRequestFileArgs(isDownloadAllData),
  );
  const requestArgs = yield call(
    withAuthorizationHeaderGenerator,
    rawRequestArgs,
  );

  yield requestDataFile({
    requestArgs: R.assoc('responseType', 'text', requestArgs),
    filename,
    pendingId: 'requestingDataFile',
  });
}

function* parseData({ data, datatype = '', queryKey }) {
  const sagaId = R.isEmpty(datatype)
    ? 'parseData'
    : `parseM${R.tail(datatype)}`;
  const key = yield select(getSagaKey(sagaId));
  if (key === queryKey && !R.isNil(queryKey)) return;

  yield put({ type: UPDATE_SAGA_KEY, sagaKey: queryKey, sagaId });

  // avoid using dynamic types to detect missing things during dev
  // ie `FLUSH_${datatype}` instead of using FLUSH_DATA or FLUSH_MICRODATA or else
  let HANDLE = HANDLE_DATA;
  if (R.equals(datatype, MICRODATA)) HANDLE = HANDLE_MICRODATA;

  const locale = yield select(getLocale);
  const freq = yield select(getFrequency);
  const dataflow = yield select(getDataflow);
  const hiddenIds = yield select(getHiddenValuesAnnotations);
  let options = {
    frequency: freq,
    locale,
    dataflowId: dataflow.dataflowId,
    hiddenIds,
    timeFormat: R.path([locale, 'timeFormat'], locales),
  };

  try {
    // isParsing start
    yield put(setPending('parsingData', true));
    yield put(flushLog(LOG_ERROR_SDMX_DATA));
    yield put({
      type: HANDLE,
      data: R.prop('data', rules.v8Transformer(data.data, options)),
      range: parseDataRange(data),
    });

    /*
     * triggers historyMiddleware to update the url with existing layout and dimensions from data
     * complete layout in location state is correct
     * partial (compact) layout in location search can't be correct without dimensions from data request
     * the following empty action is used to update location search and have a synced url
     * note 1: if pathname is not provided, existing is kept
     * note 2: if payload is empty, not override will occur (do not mix with no payload which is a reset)
     */
    yield put({ type: CHANGE_LAYOUT, replaceHistory: { payload: {} } });
    yield put(setPending('parsingData', false));
  } catch (error) {
    yield put(setPending('parsingData', false));
    yield put(pushLog({ type: LOG_ERROR_SDMX_DATA, log: error }));
  }
}

export function* watchRequestDataFile() {
  yield takeLatest(REQUEST_VIS_DATAFILE, requestVisDataFile);
}

export function* watchParseData() {
  yield takeLatest(PARSE_DATA, parseData);
  yield takeLatest(PARSE_MICRODATA, parseData);
}
