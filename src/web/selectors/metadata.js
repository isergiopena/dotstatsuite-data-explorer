import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules } from '@sis-cc/dotstatsuite-components';
import { getDataflow, getDisplay, getLocale } from './router';
import { getData } from './sdmx';
import { getDataDimensions } from './';
import { getDatasource } from '../lib/settings';
import {
  getAdvancedAttributes,
  getMetadataCoordinates,
  getHeaderCoordinates,
} from './data';

const getState = R.prop('metadata');

export const getIsOpen = createSelector(getState, R.prop('isOpen'));
export const getIsLoading = createSelector(getState, R.prop('isLoading'));

export const getCoordinates = createSelector(getState, R.prop('coordinates'));
export const getMetadataSeries = createSelector(
  getState,
  R.prop('metadataSeries'),
);
export const getIsDownloading = createSelector(
  getState,
  R.prop('isDownloading'),
);
export const getMSD = createSelector(getState, R.prop('msd'));

export const getMSDIdentifiers = createSelector(getData, data => {
  const msdDefinition = R.pipe(
    R.pathOr([], ['structure', 'annotations']),
    R.find(({ type }) => R.toLower(type) === 'metadata'),
    R.prop('title'),
  )(data);
  if (R.isNil(msdDefinition)) {
    return null;
  }
  const match = msdDefinition.match(/=([\w@_.]+):([\w@_.]+)\(([\d.]+)\)$/);
  if (R.isNil(match)) {
    return null;
  }
  const [agencyId, code, version] = R.tail(match);
  return { agencyId, code, version };
});

export const getMSDRequestArgs = createSelector(
  getDataflow,
  getLocale,
  getMSDIdentifiers,
  (dataflow, locale, identifiers) =>
    R.pipe(
      getRequestArgs,
      R.evolve({
        url: R.flip(R.concat)('/?references=conceptscheme'),
      }),
    )({
      datasource: getDatasource(R.prop('datasourceId', dataflow)),
      identifiers,
      locale,
      type: 'metadatastructure',
      withReferences: false,
    }),
);

export const getHeaderSideProps = createSelector(
  getAdvancedAttributes,
  getMetadataCoordinates,
  getHeaderCoordinates,
  (advancedAttributes, metadataCoordinates, headerCoordinates) => {
    const hasAdvancedAttributes = R.pipe(
      R.values,
      R.find(serie => {
        const mergedCoord = R.mergeLeft(serie.coordinates, headerCoordinates);
        return R.equals(mergedCoord, headerCoordinates);
      }),
      R.complement(R.isNil),
    )(advancedAttributes || {});
    const hasMetadata = R.pipe(
      R.find(coordinates => {
        const mergedCoord = R.mergeLeft(coordinates, headerCoordinates);
        return R.equals(mergedCoord, headerCoordinates);
      }),
      R.complement(R.isNil),
    )(metadataCoordinates || []);
    return hasMetadata || hasAdvancedAttributes
      ? {
          coordinates: headerCoordinates,
          hasMetadata,
          hasAdvancedAttributes,
        }
      : null;
  },
);

export const getAttributesSeries = createSelector(
  getCoordinates,
  getAdvancedAttributes,
  getDisplay,
  getDataDimensions(),
  (coordinates, advancedAttributes, display, dimensions) => {
    return R.pipe(
      R.filter(serie => {
        const mergedCoord = R.mergeLeft(serie.coordinates, coordinates);
        return R.equals(mergedCoord, coordinates);
      }),
      series =>
        R.reduce(
          (acc, serie) => {
            const attributes = R.pipe(
              R.propOr({}, 'attributes'),
              R.values,
              R.map(attribute => ({
                id: attribute.id,
                label: rules.dimensionValueDisplay(display)(attribute),
                value: rules.dimensionValueDisplay(display)(attribute.value),
              })),
            )(serie);
            const key = R.pipe(
              R.map(dim => {
                if (!R.has(dim.id, serie.coordinates)) {
                  return '';
                }
                const index = R.findIndex(
                  R.propEq('id', serie.coordinates[dim.id]),
                  dim.values,
                );
                return index;
              }),
              R.join(':'),
            )(dimensions);
            return R.assoc(key, attributes, acc);
          },
          {},
          R.values(series),
        ),
    )(advancedAttributes);
  },
);
