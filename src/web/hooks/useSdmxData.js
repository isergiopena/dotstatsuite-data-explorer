import { useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda';
import {
  getAvailableConstraintsWithoutPeriodFetched,
  getDataRequestArgs,
  getDimensions,
} from '../selectors/sdmx';
import { PARSE_DATA, FLUSH_DATA } from '../ducks/sdmx';
import useSdmxQuery from './useSdmxQuery';

export default () => {
  const dispatch = useDispatch();

  const dimensions = useSelector(getDimensions);
  const requestArgs = useSelector(getDataRequestArgs);

  const params = requestArgs?.params;
  const hasPeriod = !!(params?.startPeriod || params?.endPeriod);
  const acwpf = useSelector(getAvailableConstraintsWithoutPeriodFetched);
  const hasDimensions = !R.all(R.isEmpty)(dimensions);

  const ctx = { method: 'getData', requestArgs };

  const { isLoading, isError, error } = useSdmxQuery(ctx, {
    beforeHook: () => dispatch({ type: FLUSH_DATA }),
    successHandler: ({ data, queryKey }) =>
      dispatch({ type: PARSE_DATA, data, queryKey }),
    isEnabled: hasDimensions && (hasPeriod || acwpf),
  });

  return { isLoading, isError, error };
};
