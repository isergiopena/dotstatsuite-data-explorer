FROM node:16-alpine

ARG GIT_HASH

RUN mkdir -p /opt
WORKDIR /opt

RUN echo $GIT_HASH

COPY build /opt/build
COPY dist /opt/dist

COPY node_modules /opt/node_modules
COPY package.json yarn.lock /opt/

ENV GIT_HASH=$GIT_HASH

EXPOSE 80
CMD yarn start:run
