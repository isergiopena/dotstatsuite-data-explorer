import { select } from 'redux-saga/effects';
import { withAuthorizationHeader } from '../../api/sdmx/utils';
import { getExtAuthOptions, getToken } from '../../selectors/app';
import { getDatasource } from '../../selectors/sdmx';

export function* withAuthorizationHeaderGenerator(requestArgs) {
  // in CONFIG, datasources is an object
  // keys are datasource ids and id inside datasource object is a space id
  const { id: spaceId } = yield select(getDatasource);

  if (!spaceId) throw new Error('No space id');

  const { credentials, isAnonymous } = yield select(getExtAuthOptions(spaceId));
  const token = yield select(getToken);

  return withAuthorizationHeader({
    token,
    spaceId,
    credentials,
    isAnonymous,
  })(requestArgs);
}
