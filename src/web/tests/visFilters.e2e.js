import { changeVibe, testidSelector } from './utils';
describe('vis Filters: hierarchical selections', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('crs1');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_CRS%40DF_CRS1&df[ag]=OECD.DCD&df[vs]=1.0&av=true&pd=2019%2C2020&&dq=AUS........A`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz and open RECIPIENT filter', async () => {
    await page.waitForSelector('div[id="RECIPIENT"]');
    const filter = await page.$('div[id="RECIPIENT"]');
    await filter.click();
  });
  it('type "albania" in the spotlight', async () => {
    const spotlight = await page.$(
      `div[id="RECIPIENT"] > div > ${testidSelector('spotlight')}`,
    );
    const input = await spotlight.$(testidSelector('spotlight_input'));
    await spotlight.click();
    await page.keyboard.type('albania');
  });
  it('CEEC value should be disabled', async () => {
    const isValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(isValueDisabled).toEqual('true');
  });
  it('select albania', async () => {
    const albaniaValue = await page.$(testidSelector('value_CEEC.ALB'));
    albaniaValue.click();
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="Albania"]`,
    );
  });
  it('CEEC value not disabled anymore', async () => {
    const isValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(isValueDisabled).toEqual('false');
  });
  it('select CEEC', async () => {
    const ceecValue = await page.$(testidSelector('value_CEEC'));
    ceecValue.click();
    await page.waitForSelector(
      `${testidSelector('deleteChip-test-id')}[aria-label="CEEC"]`,
    );
  });
  it('deselect Albania', async () => {
    const albaniaValue = await page.$(testidSelector('value_CEEC.ALB'));
    albaniaValue.click();
    await page.waitFor(500);
  });
  it('CEEC should be deselected as well and re-disabled', async () => {
    const usedFilters = await page.$(testidSelector('usedFilters-vis-test-id'));
    const usedFiltersLabels = await usedFilters.$$eval(
      testidSelector('deleteChip-test-id'),
      nodes => nodes.map(n => n.ariaLabel),
    );
    const ceecValueDisabled = await page.$eval(
      testidSelector('value_CEEC'),
      node => node.ariaDisabled,
    );
    expect(usedFiltersLabels).not.toContain('CEEC');
    expect(ceecValueDisabled).toEqual('true');
  });
  it('open advanced filter pop up', async () => {
    const button = await page.$(`div[id="RECIPIENT"] > div > button`);
    button.click();
    await page.waitForSelector('div[role="dialog"]');
  });
  it('open selection mode menu', async () => {
    const button = await page.$(`button[aria-label="selection mode"]`);
    button.click();
  });
});
