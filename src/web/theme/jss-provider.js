import React from 'react';
import PropTypes from 'prop-types';
import { create } from 'jss';
import rtl from 'jss-rtl';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

export const Rtl = props => <StylesProvider jss={jss}>{props.children}</StylesProvider>;

Rtl.propTypes = {
  children: PropTypes.element,
};
