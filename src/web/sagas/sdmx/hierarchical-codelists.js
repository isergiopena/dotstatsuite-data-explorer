import * as R from 'ramda';
import { all, call, select, takeLatest, put } from 'redux-saga/effects';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import { getRawStructureRequestArgs } from '../../selectors/sdmx';
import sdmxApi from '../../api/sdmx';
import { withAuthorizationHeaderGenerator } from './utils';
import { REQUEST_ALL_HCL, HANDLE_HCLS } from '../../ducks/sdmx';

function* requestHCL(reference, datasource) {
  const { agencyId, code, version, hierarchy, codelistId } = reference;
  const rawRequestArgs = getRequestArgs({
    identifiers: { agencyId, code, version },
    datasource,
    type: 'hierarchicalcodelist',
  });
  try {
    const requestArgs = yield call(
      withAuthorizationHeaderGenerator,
      rawRequestArgs,
    );
    const response = yield call(sdmxApi, {
      method: 'getHierarchicalCodelist',
      requestArgs,
    });
    const parsed = rules2.parseHierarchicalCodelist(response, hierarchy);
    return { codelistId, hierarchy: parsed };
  } catch (_) {
    return {};
  }
}

export function* requestAllHCL({ references }) {
  const { datasource } = yield select(getRawStructureRequestArgs);
  const hierarchies = yield all(
    R.map(ref => call(requestHCL, ref, datasource), R.values(references)),
  );
  const results = R.reduce(
    (acc, { codelistId, hierarchy }) => {
      if (R.isNil(codelistId)) return acc;
      return R.assoc(codelistId, hierarchy, acc);
    },
    {},
    hierarchies,
  );
  yield put({ type: HANDLE_HCLS, hierarchies: results });
}

export function* watchRequestAllHCL() {
  yield takeLatest(REQUEST_ALL_HCL, requestAllHCL);
}
