import 'fastestsmallesttextencoderdecoder';
import { Provider } from 'react-redux';
import React from 'react';
import { createRoot } from 'react-dom/client';
import * as R from 'ramda';
import CssBaseline from '@material-ui/core/CssBaseline';
import { getLocale } from './selectors/router';
import configureStore, { history } from './configureStore';
import { I18nProvider, initialize as initializeI18n } from './i18n';
import { initialize as initializeAnalytics } from './utils/analytics';
import { ThemeProvider } from './theme';
import searchApi from './api/search';
import shareApi from './api/share';
import * as Settings from './lib/settings';
import ErrorBoundary from './components/error';
import Helmet from './components/helmet';
import meta from '../../package.json';
import { AuthPopUp as Auth } from './lib/oidc/auth';
import { OidcProvider } from './lib/oidc';
import { userSignedIn, userSignedOut, refreshToken } from './ducks/app';
import { getIsFirstRendering } from './selectors/app';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { Route, Routes } from 'react-router-dom';
import App from './components/app';
import { ReduxRouter } from '@lagunovsky/redux-react-router';
import Search from './components/search';
import Vis from './components/vis';
import NotFound from './components/not-found';
import Share from './share';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console

const initQueryClient = () => {
  return new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        refetchOnMount: false,
        retry: false,
        staleTime: 5 * 60 * 1000,
        cacheTime: 10 * 60 * 1000,
      },
    },
  });
};

const initAuth = (oidc = {}) => {
  if (R.isEmpty(oidc)) return;

  return Auth({
    authority: oidc.authority,
    client_id: oidc.client_id,
    redirect_uri: `${window.location.origin}/signed`,
    scope: R.defaultTo('openid email offline_access', oidc.scope),
    autoRefresh: true,
  });
};

const run = async () => {
  const initialState = {};

  const queryClient = initQueryClient();
  const store = configureStore(initialState);
  const auth = initAuth(window.CONFIG?.member?.scope?.oidc, store);
  const locale = getLocale(store.getState());

  searchApi.setConfig(Settings.search);
  shareApi.setConfig(window.SETTINGS?.share);
  initializeI18n(Settings.i18n, locale);

  const container = document.getElementById('root');
  const root = createRoot(container);

  const render = () => {
    if (!getIsFirstRendering(store.getState())) return;
    initializeAnalytics({ locale });

    root.render(
      <ErrorBoundary isFinal>
        <Provider store={store}>
          <OidcProvider value={auth}>
            <ThemeProvider theme={Settings.theme}>
              <I18nProvider messages={window.I18N}>
                <ErrorBoundary>
                  <Helmet />
                  <CssBaseline />
                  <ReduxRouter history={history}>
                    <QueryClientProvider client={queryClient}>
                      <App>
                        <Routes>
                          <Route path="/" element={<Search />} />
                          <Route path="/vis" element={<Vis />} />
                          <Route path="/share" element={<Share />} />
                          <Route path="*" element={<NotFound />} />
                        </Routes>
                      </App>
                      <ReactQueryDevtools initialIsOpen={true} />
                    </QueryClientProvider>
                  </ReduxRouter>
                </ErrorBoundary>
              </I18nProvider>
            </ThemeProvider>
          </OidcProvider>
        </Provider>
      </ErrorBoundary>,
    );
  };

  if (auth) {
    auth.on('signedIn', ({ access_token, payload }) => {
      const user = {
        given_name: payload.given_name,
        family_name: payload.family_name,
        email: payload.email,
      };
      store.dispatch(userSignedIn(access_token, user));
      render();
    });
    auth.on('tokensRefreshed', ({ access_token }) => {
      store.dispatch(refreshToken(access_token));
    });
    auth.on('signedOut', () => {
      store.dispatch(userSignedOut());
    });
    auth.on('error', event => {
      console.log('error', event); // eslint-disable-line no-console
      render();
    });

    await auth.init();

    window.addEventListener(
      'message',
      event => {
        if (event.origin !== window.location.origin) return;
        if (event.data?.type === 'signed') {
          auth.getAccessTokenFromCode(event.data?.code);
        }
        if (event.data?.type === 'login_required') {
          render();
        }
        if (event.data?.type === 'invalid_request') {
          console.error('invalid_request', event.data?.error_description); // eslint-disable-line no-console
        }
      },
      false,
    );

    if (window.location.pathname === '/signed') {
      const searchParams = new URLSearchParams(window.location.search);
      const error = searchParams.get('error');
      const key = error ? 'error_description' : 'code';
      const payload = {
        type: error ? error : 'signed',
        [key]: searchParams.get(key),
      };
      if (window.opener) {
        // popup
        window.opener.postMessage(payload, window.location.origin);
        window.close();
      } else {
        // iframe
        window.parent.postMessage(payload, window.location.origin);
      }

      return;
    }

    const _frame = window.document.createElement('iframe');

    // shotgun approach
    _frame.style.visibility = 'hidden';
    _frame.style.position = 'absolute';
    _frame.width = 0;
    _frame.height = 0;

    window.document.body.appendChild(_frame);

    _frame.src = `${auth.authorizationUrl}&prompt=none`;
  } else {
    render();
  }
};

run();
