import evtX from 'evtx';
import initContact from './contact';
import initCaptcha from './captcha';
import { initServices } from '../../utils';
import debug from '../../debug';

const services = [initContact, initCaptcha];

const init = ctx => {
  const api = evtX(ctx).configure(initServices(services));
  debug.info('api services up');
  return Promise.resolve({ ...ctx, services: { ...ctx.services, api } });
};

export default init;
