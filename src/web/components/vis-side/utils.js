import React from 'react';
import { FormattedMessage } from '../../i18n';

export const tagAccessor = (count, total) => (
  <FormattedMessage id="de.filter.tag" values={{ count, total }} />
);
