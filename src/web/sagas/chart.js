import * as R from 'ramda';
import { select, put, call, takeLatest, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { rules } from '@sis-cc/dotstatsuite-components';
import { getCustomAttributes, getVisChoroMap, getVisDataflow } from '../selectors';
import { getMap as getRouterMap, getViewer, getDisplay } from '../selectors/router';
import { getData } from '../selectors/sdmx';
import * as Settings from '../lib/settings';
import { CHANGE_VIEWER } from '../ducks/vis';
import { HANDLE_DATA, PARSE_STRUCTURE } from '../ducks/sdmx';
import { loadingMap, loadMapError, loadMapSuccess } from '../ducks/vis';
import { getChartConfigState } from '../selectors/chart';
import { UPDATE_ATTEMPT_CHART_CONFIG, UPDATE_CHART_CONFIG } from '../ducks/chart-configs';
import { ROUTER_ON_LOCATION_CHANGED } from '@lagunovsky/redux-react-router';

function* loadMap() {
  const routerMap = yield select(getRouterMap);
  if (R.anyPass([R.isNil, R.isEmpty])(routerMap)) {
    return;
  }
  const choroMap = yield select(getVisChoroMap);
  const { mapId, levelId } = R.pick(['levelId', 'mapId'], routerMap);
  if (
    !R.isNil(choroMap) &&
    R.prop('id', choroMap) === mapId &&
    R.prop('areaSelection', choroMap) === levelId
  ) {
    return;
  }
  yield put(loadingMap());
  try {
    if (R.isNil(mapId) || R.isNil(levelId)) {
      throw new Error('missing map entries');
    }
    const map = Settings.getMap(mapId);
    if (R.isNil(map)) {
      throw new Error('inexistant map entry');
    }
    const response = yield call(axios.get, map.path);
    const topology = response.data;
    const completeMap = R.pipe(
      R.assoc('topology', topology),
      R.assoc('areaSelection', levelId),
    )(map);
    yield put(loadMapSuccess(completeMap));
  } catch (error) {
    yield put(loadMapError(error));
  }
}

function* updateChartConfig(action) {
  let type = R.path(['pushHistory', 'payload', 'viewer'], action);
  if (!type) {
    type = yield select(getViewer);
  }
  if (type === 'overview' || type === 'table') {
    return;
  }
  const customAttributes = yield select(getCustomAttributes);
  const data = yield select(getData);
  const dataflow = yield select(getVisDataflow);
  const display = yield select(getDisplay);
  const formatterIds = R.pipe(
    R.pick(['prefscale', 'decimals']),
    R.mapObjIndexed(R.of),
  )(customAttributes);
  const configState = yield select(getChartConfigState);
  const updatedState = rules.stateFromNewProps(
    {
      data,
      dataflow,
      display,
      formatterIds,
      type,
      options: Settings.chartOptions,
    },
    configState,
  );
  yield put({ type: UPDATE_ATTEMPT_CHART_CONFIG, payload: updatedState });
}

function* handleUpdateChartConfig(action) {
  const configState = yield select(getChartConfigState);
  const refinedPayload = R.pickBy((val, key) => {
    const stateVal = R.prop(key, configState);
    return val !== stateVal;
  }, action.payload);
  if (R.isEmpty(refinedPayload)) {
    return;
  }
  yield put({ type: UPDATE_CHART_CONFIG, payload: refinedPayload });
}

export function* chartSaga() {
  yield takeLatest([ROUTER_ON_LOCATION_CHANGED, PARSE_STRUCTURE], loadMap);
  yield takeLatest([CHANGE_VIEWER, HANDLE_DATA], updateChartConfig);
  yield takeEvery([UPDATE_ATTEMPT_CHART_CONFIG], handleUpdateChartConfig);
}
