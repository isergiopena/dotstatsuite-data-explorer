const getVerb = req => req.method.toLowerCase();

const getMessage = () => {
  const service = 'healthcheck';
  const method = 'get';
  return { service, method };
};

const healthcheckConnector = evtx => (req, res, next) => {
  evtx
    .run(getMessage(req), { req })
    .then(data => res.json(data))
    .catch(next);
};

module.exports = {
  healthcheckConnector,
  getMessage,
  getVerb,
};
