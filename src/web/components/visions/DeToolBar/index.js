import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda';
import { useIntl } from 'react-intl';
import { Alert } from '@sis-cc/dotstatsuite-visions';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import Toolbar from '@material-ui/core/Toolbar';
import TuneIcon from '@material-ui/icons/Tune';
import RepeatIcon from '@material-ui/icons/Repeat';
import ShareIcon from '@material-ui/icons/Share';
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined';
import DeveloperModeOutlinedIcon from '@material-ui/icons/DeveloperModeOutlined';
import ListAltIcon from '@material-ui/icons/ListAlt';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import GridOnOutlinedIcon from '@material-ui/icons/GridOnOutlined';
import { Button } from './helpers';
import Labels from './Labels';
import Charts from './Charts';
import Downloads from './Downloads';
import { chartIsVisible } from '../../../lib/settings';
// avoid MICRODATA repetition but requires to pass as a prop to avoid import if pushed to visions
import { OVERVIEW, TABLE, MICRODATA } from '../../../utils/constants';
import { toolbarMessages } from '../../messages';
import { formatMessage, FormattedMessage } from '../../../i18n';
import { LOG_ERROR_VIS_CSV_DL, flushLog } from '../../../ducks/app';
import { csvDlStartTick } from '../../../ducks/vis';
import { getLog } from '../../../selectors/app';
import { getHasCsvDlStart } from '../../../selectors';
import { getIsDataUrlTooLong } from '../../../selectors/sdmx';

const FULLSCREEN = 'fullscreen';
const SHARE = 'share';
const CONFIG = 'config';
const API = 'api';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  warning: {
    color: 'black',
  },
}));

const Component = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const intl = useIntl();
  const csvDlLog = useSelector(getLog(LOG_ERROR_VIS_CSV_DL));
  const hasCsvDlStart = useSelector(getHasCsvDlStart);
  const isDataUrlTooLong = useSelector(getIsDataUrlTooLong);
  const [showDataUrlTooLongMessage, setShowDataUrlTooLongMessage] = useState(
    true,
  );

  // icon dry
  // usemenu dry
  // xs display
  useEffect(() => {
    if (R.and(R.isNil(props.isOpening), props.isFull)) {
      document.getElementById(FULLSCREEN).focus();
      props.changeFullscreen(true, true);
    }
  }, [props.isFull]);

  return (
    <React.Fragment>
      <Toolbar data-testid="detoolbar" className={classes.root} disableGutters>
        <div role="list" className={classes.root}>
          <div role="listitem">
            <Button
              startIcon={<ListAltIcon />}
              onClick={() => dispatch(props.changeViewer(OVERVIEW))}
              selected={R.equals(props.viewerId, OVERVIEW)}
              aria-pressed={R.equals(props.viewerId, OVERVIEW)}
              isToolTip
            >
              {formatMessage(intl)(toolbarMessages.overview)}
            </Button>
          </div>
          <div role="listitem">
            <Button
              startIcon={<TableChartOutlinedIcon />}
              onClick={() => dispatch(props.changeViewer(TABLE))}
              selected={R.equals(props.viewerId, TABLE)}
              aria-pressed={R.equals(props.viewerId, TABLE)}
              data-testid="table-button"
              isToolTip
            >
              {formatMessage(intl)(toolbarMessages.table)}
            </Button>
          </div>
          {props.hasMicrodata && (
            <div role="listitem">
              <Button
                id={MICRODATA}
                startIcon={<GridOnOutlinedIcon />}
                onClick={() => dispatch(props.changeViewer(MICRODATA))}
                selected={props.isMicrodata}
                aria-pressed={props.isMicrodata}
                disabled={R.not(props.hasMicrodataData)}
                isToolTip
              >
                {formatMessage(intl)(toolbarMessages.microdata)}
              </Button>
            </div>
          )}
          {chartIsVisible && (
            <div role="listitem">
              <Charts
                availableCharts={props.availableCharts}
                chart={props.viewerId}
                changeChart={(type, options) =>
                  dispatch(props.changeViewer(type, options))
                }
                selected={
                  !R.any(R.equals(props.viewerId), [TABLE, MICRODATA, OVERVIEW])
                }
                hasRefAreaDimension={props.hasRefAreaDimension}
              />
            </div>
          )}
        </div>
        <div role="list" className={classes.root}>
          <div role="listitem">
            <Labels
              label={props.dimensionGetter}
              changeLabel={id => dispatch(props.changeDisplay(id))}
            />
          </div>
          {!props.isMicrodata && !props.isOverview && (
            <div role="listitem">
              <Button
                data-testid="customize"
                startIcon={props.isTable ? <RepeatIcon /> : <TuneIcon />}
                onClick={() => dispatch(props.changeActionId(CONFIG))}
                aria-expanded={R.equals(props.actionId, CONFIG)}
                selected={R.equals(props.actionId, CONFIG)}
                isToolTip
              >
                {props.isTable
                  ? formatMessage(intl)(toolbarMessages.layout)
                  : formatMessage(intl)(toolbarMessages.customize)}
              </Button>
            </div>
          )}
          {window.SETTINGS?.share?.endpoint &&
            !props.isMicrodata &&
            !props.isOverview && (
              <div role="listitem">
                <Button
                  startIcon={<ShareIcon />}
                  onClick={() => dispatch(props.changeActionId(SHARE))}
                  selected={R.equals(props.actionId, SHARE)}
                  aria-expanded={R.equals(props.actionId, SHARE)}
                  isToolTip
                >
                  {formatMessage(intl)(toolbarMessages.share)}
                </Button>
              </div>
            )}
          <div role="listitem">
            <Downloads
              loading={props.isDownloading}
              viewerProps={props.viewerProps}
            />
          </div>
          {!props.isMicrodata && (
            <div role="listitem">
              <Button
                id={API}
                startIcon={<DeveloperModeOutlinedIcon />}
                onClick={() => dispatch(props.changeActionId(API))}
                selected={R.equals(props.actionId, API)}
                aria-expanded={R.equals(props.actionId, API)}
                isToolTip
              >
                {formatMessage(intl)(toolbarMessages.apiqueries)}
              </Button>
            </div>
          )}
          <div role="listitem">
            <Button
              id={FULLSCREEN}
              startIcon={
                props.isFull ? <FullscreenExitIcon /> : <FullscreenIcon />
              }
              onClick={() =>
                dispatch(props.changeFullscreen(R.not(props.isFull)))
              }
              selected={props.isFull}
              aria-pressed={props.isFull}
              isToolTip
            >
              {formatMessage(intl)(toolbarMessages.fullscreen)}
            </Button>
          </div>
          {/*<More actionId={props.actionId} changeActionId={props.changeActionId} />*/}
        </div>
      </Toolbar>
      {csvDlLog && (
        <Alert
          color="error"
          variant="filled"
          onClose={() => dispatch(flushLog(LOG_ERROR_VIS_CSV_DL))}
        >
          <strong>
            <FormattedMessage id="data.download.csv.error" />
          </strong>
          {R.pathOr('', ['log', 'error', 'message'], csvDlLog)}
        </Alert>
      )}
      {hasCsvDlStart && (
        <Alert
          icon={<GetAppIcon />}
          color="success"
          variant="filled"
          onClose={() => dispatch(csvDlStartTick())}
        >
          <strong>
            <FormattedMessage id="data.download.csv.launch" />
          </strong>
          <FormattedMessage id="data.download.csv.launch.detail" />
        </Alert>
      )}
      {isDataUrlTooLong && showDataUrlTooLongMessage && (
        <Alert
          className={classes.warning}
          severity="warning"
          variant="filled"
          onClose={() => setShowDataUrlTooLongMessage(false)}
        >
          <FormattedMessage id="data.warning.url.too.long" />
        </Alert>
      )}
    </React.Fragment>
  );
};

Component.propTypes = {
  availableCharts: PropTypes.object.isRequired,
  changeActionId: PropTypes.func.isRequired,
  viewerId: PropTypes.string,
  actionId: PropTypes.string,
  changeViewer: PropTypes.func.isRequired,
  changeDisplay: PropTypes.func.isRequired,
  changeFullscreen: PropTypes.func.isRequired,
  dimensionGetter: PropTypes.string,
  isFull: PropTypes.bool,
  isOpening: PropTypes.bool,
  isDownloading: PropTypes.bool,
  hasRefAreaDimension: PropTypes.bool,
  externalResources: PropTypes.array,
  hasMicrodata: PropTypes.bool,
  isMicrodata: PropTypes.bool,
  hasMicrodataData: PropTypes.bool,
  isOverview: PropTypes.bool,
  isTable: PropTypes.bool,
  viewerProps: PropTypes.object,
};

export default Component;
